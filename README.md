# WikiPrizm

This repository contains community documentation for the Casio Prizm calculator,
primarily targeting software development. Much of the information herein has
been reverse-engineered, and is collected here to make it much easier to find.

The documentation is published to **https://prizm.cemetech.net/**: that is the
place to go to read the documentation, whereas this is where changes can be made.

## Contributing

Contributions of all kinds are welcome. If you have not already been granted
collaborator permissions allowing you to merge changes to this repository, you
can fork it and make changes in a branch, then open a Merge Request to get your
changes integrated into this repository.
Changes merged to the `main` branch will automatically be built and deployed.

The site content itself lives in the [`content`](content/) directory. Files
outside that directory should not often need to be changed.

### Building the site

The site is built with [Hugo](https://gohugo.io/). After having installed Hugo,
simply run `hugo` in this repository's root directory to build the web site into
the `public` directory. To test changes, `hugo serve` is useful because it will
allow you to open the site in a browser and make changes, automatically reloading
your browser to get immediate feedback on how your changes look.

### Adding new pages

Although you can simply create new files by hand to create new pages, it is
often preferable to use `hugo new` to do so, because that will use a
template for the relevant kind of page which will often make it easier to
fill in with information.

This command will create a new file in `content/Syscalls/SayHello.md` using the
template for a syscall documentation page, for example:

   hugo new Syscalls/SayHello.md

### Best practices

When linking between pages, use the [`ref` shortcode](https://gohugo.io/content-management/cross-references/).
This helps ensure that your links will work, because Hugo will complain if you
specify a link target with `ref` that it cannot resolve to an existing page
instead of letting you create a broken link.

If moving a page, add an [alias](https://gohugo.io/content-management/urls/#aliases)
for its old location so old links continue to work. Searching for `aliases:` in
existing pages should make it easy to find examples.

## History

WikiPrizm was formerly run as a traditional wiki using [MediaWiki](https://www.mediawiki.org/)
and operated on the Cemetech server.
The scripts and raw data used to generate the version in this repository are retained, making
it relatively easy to inspect the original pages in case some earlier history needs to
be viewed.

 * [`prizmcemetechnet-20221129-history.xml`](prizmcemetech.net-20221129-history.xml)
   is the MediaWiki XML dump from which pages were extracted.
   This contains the text of all pages that existed at the time the dump was created as well as metadata
   for the most recent version of each page at that time.
 * [`convert.py`](convert.py) is the script that consumes the XML dump and writes markdown files. It uses
   [pandoc](https://pandoc.org/) and several other Python libraries to complete this task.
    * [`relref_underscore_rewrite.py`](relref_underscore_rewrite.py) implements several pandoc filters that improve the results
      of conversion to be easier to read.
    * [`pyproject.toml`](pyproject.toml) and [`poetry.lock`](poetry.lock) provide metadata regarding what's required to run convert.py,
      understood by [`poetry`](https://python-poetry.org/). The script can be easily executed by first
      running `poetry install` to install the dependencies, then `poetry run python convert.py` to run it.
