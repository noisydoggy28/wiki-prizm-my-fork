---
title: StrRight(
---

## StrRight(

## Description

This command reads a number of letters from the string's ed(the right
part of it)

## Syntax

`StrRight(<String name>,<number of letters to read>)`

## Example

    "CASIO PRIZM" → Str 1
    Locate 1,1,StrRight(Str 1,5)                    //Displays the last 5 characters of the string(PRIZM)
