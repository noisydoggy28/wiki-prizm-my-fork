---
revisions:
- author: YeongJIN COOL
  comment: /\* Syntax \*/
  timestamp: '2012-02-16T00:02:55Z'
title: CellMin(
---

# CellMin(

## Description

This command returns the minimum value in a specified cell range.

## Syntax

**CellMin(***start_cell*:*end_cell***)**

## Example

`CellMin(A3:C5)`
