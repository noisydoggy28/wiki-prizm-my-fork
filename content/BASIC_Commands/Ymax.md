---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= YMax = == Description == This command\nsets\
    \ the maximum y value displayed in the graphscreen. == Syntax\n==\u2019\u2018\
    Value\u2019\u2018\u2192YMax == Example == 3\u2192\u2019\u2018\u2019Ymax\u2019\u2019\
    \u2019\n\\[\\[Category:BASIC_Com\u2026\u2019"
  timestamp: '2012-02-24T20:31:08Z'
title: Ymax
---

# YMax

## Description

This command sets the maximum y value displayed in the graphscreen.

## Syntax

*Value*→YMax

## Example

`3→`**`Ymax`**
