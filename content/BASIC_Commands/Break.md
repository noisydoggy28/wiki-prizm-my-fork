---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Break = == Description == This command\nis used\
    \ to break out of the loop. == Syntax ==\u2019\u2018\u2019Break\u2019\u2019\u2019\
    \ == Example\n== For 1\u2192A To 21 If A=3 Then Break IfEnd Next\u2019"
  timestamp: '2012-02-17T00:48:19Z'
title: Break
---

# Break

## Description

This command is used to break out of the loop.

## Syntax

**Break**

## Example

    For 1→A To 21
    If A=3
    Then
    Break
    IfEnd
    Next
