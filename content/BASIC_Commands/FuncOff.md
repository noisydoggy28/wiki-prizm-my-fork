---
revisions:
- author: Turiqwalrus
  comment: /\* Description \*/
  timestamp: '2012-02-29T20:22:13Z'
title: FuncOff
---

# FuncOff

## Description

This command sets the OS so it doesn't draws functions on the
graphscreen.

## Syntax

**FuncOff**

## Example

`FuncOff`
