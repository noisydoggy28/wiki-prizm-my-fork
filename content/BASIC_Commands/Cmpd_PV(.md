---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Cmpd_PV( = == Description == This\ncommand returns\
    \ present value(loan amount or principal) == Syntax\n==\u2019\u2018\u2019Cmpd_PV(\u2019\
    \u2019\u2019\u2018\u2019n\u2019\u2018,\u2019\u2018I%\u2019\u2018,\u2019\u2018\
    PMT\u2019\u2018,\u2019\u2018FV\u2019\u2018,\u2019\u2018P/Y\u2019\u2018,\u2019\u2018\
    C/Y\u2019\u2019\u2019\u2019\u2018)\u2019\u2019\u2019\n== Exam\u2026\u2019"
  timestamp: '2012-02-16T00:12:59Z'
title: Cmpd_PV(
---

# Cmpd_PV(

## Description

This command returns present value(loan amount or principal)

## Syntax

**Cmpd_PV(***n*,*I%*,*PMT*,*FV*,*P/Y*,*C/Y***)**

## Example

`Cmpd_PV(10,5.5,10,0,12,12)`
