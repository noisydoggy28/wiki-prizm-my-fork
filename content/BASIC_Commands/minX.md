---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= minX = == Description == This is for\nspecifying\
    \ minimum x value in x axis at graphscreen. == Syntax\n==\u2019\u2018Value\u2019\
    \u2018\u2192\u2019\u2018\u2019MinX\u2019\u2019\u2019 == Example == 3\u2192MinX\u2019"
  timestamp: '2012-02-17T01:00:50Z'
title: minX
---

# minX

## Description

This is for specifying minimum x value in x axis at graphscreen.

## Syntax

*Value*→**MinX**

## Example

`3→MinX`
