---
revisions:
- author: Purobaz
  comment: "Created page with \u2018= BG-None = == Description == This\ncommand removes\
    \ the screen background. But it doesn\u2019t clear the\nscreen. == Syntax ==\u2019\
    \u2018\u2019BG-None\u2019\u2019\u2019 == Example == BG-None Cls\n\\[\\[Category:BA\u2026\
    \u2019"
  timestamp: '2012-02-22T22:35:06Z'
title: BG-None
---

# BG-None {#bg_none}

## Description

This command removes the screen background. But it doesn't clear the
screen.

## Syntax

**BG-None**

## Example

    BG-None
    Cls
