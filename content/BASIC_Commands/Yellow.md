---
title: Yellow
---

# Yellow

## Description

This command sets the subsequent Locate or other text command to render
in yellow.

## Syntax

**Yellow \<text command>**

## Example

`Yellow Locate 1, 1, "Asdf`
