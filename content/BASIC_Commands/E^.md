---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= e^ = == Description == This command\ncalculates\
    \ the value of e^value. == Syntax ==\u2019\u2018\u2019e^\u2019\u2019\u2019\u2018\
    \u2019value\u2019\u2019 ==\nExample == e^5 \\[\\[Category:BASIC_Commands\\]\\\
    ]\u2019"
  timestamp: '2012-02-15T20:06:35Z'
title: E^
---

# e^

## Description

This command calculates the value of e^value.

## Syntax

**e^***value*

## Example

`e^5`
