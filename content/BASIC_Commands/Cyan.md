---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Cyan = == Description == This command\nsets\
    \ the subsequent Locate or other text command to render in cyan.\nThis command\
    \ can be found at \\[Shift\\] \\[5\\] \\[1\\]. == Syntax\n==\u2019\u2018\u2019\
    Cyan \\<t\u2026\u2019"
  timestamp: '2012-02-15T12:06:22Z'
title: Cyan
---

# Cyan

## Description

This command sets the subsequent Locate or other text command to render
in cyan. This command can be found at \[Shift\] \[5\] \[1\].

## Syntax

**Cyan \<text command>**

## Example

`Cyan Locate 1, 1, "Asdf`
