---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-29T20:01:35Z'
title: Max(
---

# Max(

## Description

This command returns the maximum value from the given numbers.

## Syntax

**Max(***List***)**

## Example

`Max(List1`
