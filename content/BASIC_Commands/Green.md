---
title: Green
---

# Green

## Description

This command sets the subsequent Locate or other text command to render
in green.

## Syntax

**Green \<text command>**

## Example

`Green Locate 1, 1, "Asdf`
