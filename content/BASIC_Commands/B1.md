---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= b1 = == Description == This is the b1\nvalue\
    \ used in recursion. Can be used like the other variables. ==\nSyntax ==\u2019\
    \u2018\u2019b1\u2019\u2019\u2019 == Example == b1 ?\u2192b1 \\[\\[Category:BASIC_Comman\u2026\
    \u2019"
  timestamp: '2012-02-15T20:04:10Z'
title: B1
---

# b1

## Description

This is the b1 value used in recursion. Can be used like the other
variables.

## Syntax

**b1**

## Example

`b1`

`?→b1`
