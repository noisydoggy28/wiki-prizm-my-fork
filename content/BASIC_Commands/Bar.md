---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Bar = == Description == This sets the\nstat\
    \ graph type to Bar graph. == Syntax ==\u2019\u2018\u2019Bar\u2019\u2019\u2019\
    \ == Example ==\nBar \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-21T00:17:24Z'
title: Bar
---

# Bar

## Description

This sets the stat graph type to Bar graph.

## Syntax

**Bar**

## Example

`Bar`
