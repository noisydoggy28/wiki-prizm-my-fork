---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= CellSum( = == Description == This\ncommand returns\
    \ the sum of the data in a specified cell range. ==\nSyntax ==\u2019\u2018\u2019\
    CellSum(\u2019\u2019\u2019\u2018\u2019start_cell\u2019\u2018:\u2019\u2018end_cell\u2019\
    \u2019\u2019\u2019\u2018)\u2019\u2019\u2019 == Example\n== \u2026\u2019"
  timestamp: '2012-02-16T00:05:31Z'
title: CellSum(
---

# CellSum(

## Description

This command returns the sum of the data in a specified cell range.

## Syntax

**CellSum(***start_cell*:*end_cell***)**

## Example

`CellSum(A3:C5)`
