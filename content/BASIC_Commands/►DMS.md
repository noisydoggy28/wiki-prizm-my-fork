---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= \u25BADMS = == Description == This command\n\
    converts value to degree-minute-second format. == Syntax\n==\u2019\u2018Value\u2019\
    \u2019\u2019\u2019\u2018\u25BADMS\u2019\u2019\u2019 == Example == 30\u25BADMS\n\
    \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-15T03:37:55Z'
title: "\u25BADMS"
---

# ►DMS

## Description

This command converts value to degree-minute-second format.

## Syntax

*Value***►DMS**

## Example

`30►DMS`
