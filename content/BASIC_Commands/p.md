---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= P = == Description == This is the\ncommand to\
    \ get a permutation. == Syntax ==\u2019\u2018n\u2019\u2019\u2019\u2018\u2019P\u2019\
    \u2019\u2019\u2018\u2019r\u2019\u2019 ==\nExample == 3P2 \\[\\[Category:BASIC_Commands\\\
    ]\\]\u2019"
  timestamp: '2012-02-16T23:14:02Z'
title: p
---

# P

## Description

This is the command to get a permutation.

## Syntax

*n***P***r*

## Example

`3P2`
