---
revisions:
- author: Turiqwalrus
  comment: /\* Syntax \*/
  timestamp: '2012-02-29T18:05:48Z'
title: cos
---

## Description

calculates the cosine of the inputted number

## Syntax

cos `<real number>`{=html}

## Example

cos 60 //calculates the cosine of 60(the result depends on whether the
calculator is set to measure in degrees or radians)
