---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= cos\xAF\xB9 = == Description == This command\n\
    returns the arccosine of the value. == Syntax ==\u2019\u2018\u2019cos\xAF\xB9\u2019\
    \u2019\u2019\n\u2018\u2019Value\u2019\u2019 == Example == cos\xAF\xB9 .75\u2019"
  timestamp: '2012-02-29T20:21:12Z'
title: "cos\xAF\xB9"
---

# cos¯¹

## Description

This command returns the arccosine of the value.

## Syntax

**cos¯¹** *Value*

## Example

`cos¯¹ .75`
