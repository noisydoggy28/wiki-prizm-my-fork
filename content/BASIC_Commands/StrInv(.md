---
title: StrInv(
---

## StrInv(

## Description

Inverts the inputted string.

## Syntax

StrInv(\<String name>)

## Example

    "EMAG EHT" → Str 1
    Locate 1,1,StrInv(Str 1)       //Returns the inverted string("THE GAME")
