---
title: User Name Registry
aliases:
  - /User_Name_Registry/
---

[CASIOWIN]({{< ref "_index.md" >}}) supports registering the name and
organization of the calculator owner for easy identification. This
information is shown on the bottom right corner of the screen, along
with the Casio logo, when the calculator is entering the normal
[standby]({{< ref "_index.md#standby" >}}) mode. Registering this information
can be done in the built-in "System" app, by entering it, pressing F6
and then F2.

The data is supposedly protected using a password that can have up to
eight characters. The "User Name" and "Organization" fields can have up
to 19 characters each. If there is data registered already, the user is
prompted for the password before the other fields can be edited, and the
same if the user wishes to delete the data so that nothing is shown when
entering standby.

## Effective security and privacy concerns {#effective_security_and_privacy_concerns}

What most people don't know about this feature, and isn't explained in
the calculator manual, is that:

-   It's incredibly easy to find out the password - this can be done by
    using specialized custom add-ins, looking at the relevant memory
    addresses with a generic viewer like INSIGHT, or even without any
    external or custom software, by using [secret key
    combinations]({{< ref "Secret_Key_Combinations.md" >}}) to access
    the [TestMode]({{< ref "Syscalls/TestMode.md" >}}), then using its
    memory viewer to look at the relevant addresses;
-   It's incredibly easy to clear the existing information so that new
    identifications can be registered without entering a password
    (again, key combinations are your friend);
-   The whole history of user names, organizations and passwords ever
    registered is stored in the [Flash]({{< ref "Flash.md" >}}) memory,
    and doesn't go away when "deleting" the registered information from
    the System menu or using the key combination to "clear" the
    information without needing to enter a password. All the OS-provided
    functions to clear the information - including the key combination
    one - do, is adding an empty entry to the history of user names and
    passwords, preserving existing entries. Again, retrieving the
    information is like described in the previous point;
-   Knowledgeable people will also know how to delete this history (it's
    [described in this Wiki]({{< ref "Secret_Key_Combinations.md" >}})),
    so chances are you can't even use it to prove that a specific Prizm
    was yours at a certain point in time.

The lessons to take away are:

-   Do not bet the security / "ownership" of your Prizm on the user name
    registration feature. It is only useful to distinguish between
    identical Prizms close to each other (as in a classroom setting),
    and to store contact information so that in case the calculator is
    lost and someone honest finds it, it can be returned to its owner;
-   Since the password is so easy to retrieve or simply bypass, and
    given that the whole history of user names, organizations and
    passwords is stored (unless very specific action in contrary is
    taken), you should not register sensitive information (e.g., do not
    use a password you would use in any other place).

## Technical details {#technical_details}

The OS dedicates a whole [Flash]({{< ref "Flash.md" >}}) sector, with a
size of 128 KiB, just for registering user names, organizations and
passwords (in plain text, of course). This means the OS uses one of the
256 flash sectors, or roughly 0.4% of the flash capacity, to store
something that under normal conditions (i.e., no log kept) would take
19+19+8 = 46 bytes to store (or less than 0.0002% of the flash
capacity).

The flash sector in question, sometimes called "password area", is
located in the address range 0xA0BE0000 to 0xA0C00000. New entries are
added to the log at its end, so that the location of the current entry
grows upwards as more entries are registered. When entering standby, the
OS might iterate over the records until it finds the latest one, or it
may store a pointer to the latest record somewhere.

Entries are spaced by 0x40 bytes, with the organization field stored
with a offset of 4 bytes from the multiple of 0x40, the user name with a
offset of 0x18 bytes and the password with a offset of 0x2C bytes.

When the log fills up (and it takes 2048 entries before it does), it is
erased, and the registering continues from the beginning. Fortunately,
this is not the only way to erase the log.

## Deleting the user information log {#deleting_the_user_information_log}

Other than by filling up the log, the OS doesn't appear to provide any
way for the log to be cleared. Fortunately, the
[bootloader]({{< ref "CASIOABS.md" >}}) has some service menus, and can
be [used to erase the user information
sector]({{< ref "Secret_Key_Combinations.md" >}}).
