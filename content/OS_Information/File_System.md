---
revisions:
- author: Gbl08ma
  comment: "It\u2019s almost certain that the filesystem is the one from\nFugue, update\
    \ text to reflect this"
  timestamp: '2015-02-12T21:34:55Z'
title: File System
aliases:
  - /File_System/
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

The Prizm has two separate file systems, in RAM and in Flash.

## Main Memory {#main_memory}

The *Main Memory* is a 61KB region in the RAM (with multiple backed up
versions in Flash, possibly in a rolling setup) that is used for saving
config files, programs, and other data that is modified often. Casio
calls it the **MCS**, possibly an acronym for "Main Content System".

Files in the MCS can have any kind of content, but they do not have an
extension. Using the default OS means, users can't directly control what
each file contains, nor create arbitrary files (except for programs,
which can have relatively arbitrary content), nor decide in which folder
files go - even when connecting through USB, the contents of the
\@MainMem abstraction are not directly mapped to the contents of the
MCS!

Folders are supported, but sub-folders are not. Rather than a full file
system, the MCS can be thought of as a key-value store, or an
associative array, where each key is the conjunction of the folder name
and the file name (but keep in mind that, internally, it is not stored
as a traditional C array!).

One can inspect the true, unfiltered contents of the MCS using an add-in
like Insight or the [TestMode]({{< ref "Syscalls/TestMode.md" >}}). It
will show up a bunch of folders not normally visible, some because they
are empty and others because they are system folders. It will also show
that the user-facing MCS browsers filter a lot of things and even rename
some folders: for example, the "system" folder, where BASIC programs are
located, is shown with the name of "PROGRAM".

The [MCS\_\* functions]({{< ref "Syscalls/MCS/" >}}) are used to access
the Main Memory.

## Storage Memory {#storage_memory}

The *Storage Memory* is a 16MB region in [flash]({{< ref "flash.md" >}})
that contains data in many formats. When the Prizm is connected via a
USB cable, the Storage Memory is presented (along with a folder for
limited access to the Main Memory) as a USB drive.

The filesystem in the Storage Memory is a FAT16 with a directory
structure very similar to PC DOS. Directories do not reside at fixed
memory positions. The filesystem used is the Fugue Flash filesystem from
Kyoto Software Research; the following string can be found the a Prizm
at various memory locations: "Fugue FAT File System 2004-03-03 00:00". A
PDF about this filesystem can be found here:
[1](http://www.venture-mekiki.jp/catalog/Fugue.pdf).

The [Bfile\_\* functions]({{< ref "Syscalls/Bfile/" >}}) are used to
access files in the Storage Memory. For Bfile\_\* functions, you must
start off all paths with "\\\\fls0\\" (which is a special prefix that
specifies that the built-in filesystem will be used). *Note that in C,
the escaped form would be "\\\\\\\\fls0\\\\"*. While the Prizm doesn't
at this point have the ability to interact with SD cards, the prefix for
accessing the filesystem contained on one would be "\\\\crd0\\", or
"\\\\\\\\crd0\\\\" in C.
