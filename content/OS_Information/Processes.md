---
revisions:
- author: Gbl08ma
  timestamp: '2014-12-04T00:48:14Z'
title: Processes
aliases:
  - /Processes/
  - /Stack/
  - /Heap/
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

While not a multitasking operating system, the Prizm's
[OS]({{< ref "_index.md" >}}) appears to have support for a fixed
amount of "processes", which only run when in foreground. A process
usually comes into foreground on user request.

The amount of processes supported appears to be two. One is the *main*
process, and the other is the *child* process. All built-in OS apps and
add-ins run as the child process, except eActivity, which runs as the
main process. When a strip inside a eActivity file is open, it runs as
the child process, and alternating between the strip and the eActivity
document equates to selecting what process is in foreground.

When another app or add-in is selected on the Main Menu, or when another
strip is launched (or the running app inside the strip is changed), the
old app in the child process terminates, and the new one takes place.
[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) appears to play an
important role in process switching, as it is one of the few syscalls
that handle proper Main Menu access and switching between a strip and
the eActivity document.

It is not clear in which process things like the Main Menu or screenshot
capture screen run - it is actually possible that there's a third
process set up for the Main Menu.

## Stack

Each process has a different stack.

The main process' stack has a size of 458751 bytes, in the address range
0x880F0000 to 0x8815FFFF on the CG10/20, or 0x8C0F0000 to 0x8C15FFFF on the CG50.

The child process' stack, which is the one normally used, has a size of
524288 bytes, in the address range 0x88160000 to 0x881DFFFF (0x8C160000 to 0x8C1DFFFF on the CG50). This is
also used by the static add-in RAM (static RAM comes from the start, the
stack grows downwards, from the end). The start of the child process'
stack is virtualized to 0x08100000, at least in the case of add-ins.

Stacks, along with the [heap](#heap), are saved to flash
on power off and restored when turning back on, meaning the power off
actually works more like hibernation.

## Heap

The Prizm OS has at least one heap, with a size of about 128 KB, located in the
address range 0x881E0000 to 0x881FFFFF on the CG10/20, or 0x8C1E0000 to 0x8C1FFFFF on the CG50.

The implementation appears to have some bugs; namely, one can't allocate
one big buffer at the first try, and it appears to not manage free
blocks properly, making fragmentation go too high at a certain point and
basically making the heap useless. If the heap is used heavily, it may
also get to a point (usually after freeing many buffers) where the OS
reports there's more heap available than the total heap size - and
usually the values are quite out of range, like 10 MB. There is some
more discussion on [this forum
thread](http://www.cemetech.net/forum/viewtopic.php?t=7539).

The heap has the usual set of [related
syscalls]({{< ref "Syscalls/Heap/" >}}).

On the CG50, the heap expands to 3MB (3072KB) when running the built-in Python add-in. This can be confirmed by opening [Test mode]({{< ref "/OS_Information/Secret_Key_Combinations.md" >}} in the application and opening "STACK/HEAP REMAIN". The start of the heap remains the same, but the end address changes to 0x8C4DFFFF, utilizing some of the extra RAM available on this model. Add-ins cannot use this heap area via the usual malloc and free syscalls, but it is possible to write directly into its memory area, starting at 0x8C200000 to avoid the 128KB heap. Using this may be unsafe if Casio uses memory area for something else in the future so it is recommended to check the OS version and confirm that the area is zeroed out before using it.

The [stack](#stack) can provide a bigger continuous RAM
area than the heap (up to about 500 KiB, depending on static RAM
requirements, versus about 128 KiB). This, plus the limitations
described above, means that to get the most out of the memory available
to add-ins, one has to use non-standard (*"incorrect"*) practices such
as preferably using the stack instead of the heap.
