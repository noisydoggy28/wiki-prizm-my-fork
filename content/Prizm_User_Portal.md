---
title: Prizm User Portal
---

If you are trying to figure out how to get the Prizm to do something,
either a system app, Basic function, or something else, make sure that
you check out the [fx-CG10/20 user
manual](https://support.casio.com/en/manual/manualfile.php?cid=004008007)
from Casio.

Contents coming soon. For now:

-   Downloads: [Prizm
    Archives](https://www.cemetech.net/downloads/browse/prizm/)
    (and in particular, [Prizm
    Games](https://www.cemetech.net/downloads/browse/prizm/games/))
-   [How do I put games on my Casio
    Prizm?](http://www.cemetech.net/forum/viewtopic.php?t=7549)
-   Other help: [Prizm
    Subforum](http://www.cemetech.net/forum/viewforum.php?f=68)

## Tutorials

Coming soon. Want to contribute? Visit the Prizm Subforum linked above.
