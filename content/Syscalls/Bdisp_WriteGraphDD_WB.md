---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Bdisp_WriteGraphDD_WB\n\\\
    | index = 0x0291 \\| signature = void Bdisp_WriteGraphDD_WB(struct\ndisplay_graph\\\
    * gd) \\| header = fxcg/display.h \\| parameters = \\*\n\u2019\u2019struc\u2026\
    \u201D"
  timestamp: '2014-07-29T22:41:14Z'
title: Bdisp_WriteGraphDD_WB
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0291\
**Function signature:** void Bdisp_WriteGraphDD_WB(struct
display_graph\* gd)

Directly draws a bitmap to screen, bypassing VRAM. Similar to
[Bdisp_WriteGraphVRAM]({{< ref "Syscalls/Bdisp_WriteGraphVRAM.md" >}}).\

## Parameters

-   *struct display_graph\** **gd** - pointer to struct containing
    information on the bitmap to draw. See
    [Bdisp_WriteGraphVRAM]({{< ref "Syscalls/Bdisp_WriteGraphVRAM.md" >}})
    for more information. The only difference is that, with
    Bdisp_WriteGraphDD_WB, the y value is automatically incremented by
    24 pixels, to skip the status area.
