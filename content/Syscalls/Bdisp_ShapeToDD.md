---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Bdisp_ShapeToDD \\|\nindex\
    \ = 0x01C0 \\| signature = void Bdisp_ShapeToDD(struct\ndisplay_shape \\* shape,\
    \ int color) \\| header = fxcg/display.h \\|\nsynopsis = Draws a\u2026\u201D"
  timestamp: '2014-07-29T12:03:21Z'
title: Bdisp_ShapeToDD
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x01C0\
**Function signature:** void Bdisp_ShapeToDD(struct display_shape \*
shape, int color)

Draws a shape directly to the display, bypassing VRAM.\

## Parameters

-   **shape** - shape to draw

    ```
    struct display_shape {
        int dx;
        int dy;
        int wx;
        int wy;
        int color;
        void* saved;
    };
    ```

-   **color** - color of the shape.
