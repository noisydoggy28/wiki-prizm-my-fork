---
revisions:
- author: Dr-carlos
  comment: Add description of what DrawFrameWorkbench does
  timestamp: '2022-03-26T05:14:03Z'
title: DrawFrameWorkbench
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0923\
**Function signature:** void DrawFrameWorkbench(int x1, int y1, int x2,
int y2, int color)

Clears the specified VRAM and draws a rectangle to it, avoiding the
status area.\

## Parameters

-   **x1** - horizontal coordinate of the top-left corner of the
    rectangle, 0 to 383.
-   **y1** - vertical coordinate of the top-left corner of the
    rectangle, 0 to 191. 24 pixels are automatically added to bypass the
    status area.
-   **x2** - horizontal coordinate of the bottom-right corner of the
    rectangle, 0 to 383.
-   **y2** - vertical coordinate of the bottom-right corner of the
    rectangle, 0 to 191. 24 pixels are automatically added to bypass the
    status area.
-   **color** - Line color of the rectangle, as listed in note 3 of
    [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}).\

## Comments

This syscall uses
[Bdisp_Rectangle]({{< ref "Syscalls/Bdisp_Rectangle.md" >}}) to draw the
rectangle, and [Bdisp_AreaClr_VRAM_WB](Bdisp_AreaClr_VRAM_WB) to clear
the VRAM underneath the rectangle, neither of which are currently
included in libfxcg.
