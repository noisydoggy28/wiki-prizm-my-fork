---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-04T12:07:55Z'
title: EditMBStringChar
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x1224\
**Function signature:** int EditMBStringChar(unsigned char\* MB_string,
int posmax, int xpos, int char_to_insert)

Edits a [multi-byte string]({{< ref "Multi-byte_strings.md" >}})
according to the specified character key (where the GetKey code is lower
than 30000) and cursor position. To be used in conjunction with
[DisplayMBString]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString.md" >}})
and
[EditMBStringCtrl]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringCtrl.md" >}}).\

## Parameters

-   **MB_string** - pointer to multi-byte string to edit. Must not be
    constant, as this syscall will modify the string contents, inserting
    a character.
-   **posmax** - maximum size, in bytes, the string can take; the size
    of the buffer **MB_string** points to should be taken into account,
    to avoid buffer overflows.
-   **xpos** - current cursor position (see the
    [DisplayMBString]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString.md" >}})
    **cursor** parameter).
-   **char_to_insert** -the
    [GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) code of the
    character key pressed.\

## Returns

The new cursor position - set the variable used on the **xpos**
parameter to this value to advance the cursor.\

## Comments

This syscall is for when a character key is pressed during text editing.
If the key pressed is not a character key, or in other words, if the
GetKey code is equal or higher than 30000, use
[EditMBStringCtrl]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringCtrl.md" >}})
instead.
