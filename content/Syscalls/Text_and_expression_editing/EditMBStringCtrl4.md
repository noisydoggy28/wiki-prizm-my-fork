---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = EditMBStringCtrl4 \\|\nheader\
    \ = fxcg/keyboard.h \\| index = 0x120C \\| signature = void\nEditMBStringCtrl4(unsigned\
    \ char*, int xposmax, void*, void*, void*,\nint, in\u2026\u201D"
  timestamp: '2014-07-31T15:39:16Z'
title: EditMBStringCtrl4
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x120C\
**Function signature:** void EditMBStringCtrl4(unsigned char\*, int
xposmax, void\*, void\*, void\*, int, int, int, int, int, int)

The exact function of this syscall is not yet known; it is most probably
an even more overloaded variant of
[EditMBStringCtrl]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringCtrl.md" >}}).\

## Parameters

The meaning of the parameters is not known.
