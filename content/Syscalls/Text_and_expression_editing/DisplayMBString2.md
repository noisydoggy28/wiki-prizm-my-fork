---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-04T12:05:32Z'
title: DisplayMBString2
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x1218\
**Function signature:** `void DisplayMBString2(int select, unsigned char *str, int start, int xpos, int sel_start, int x, int y, int pos_to_clear, int right_margin, int key)`

Draws a string to VRAM, possibly containing [multi-byte
characters]({{< ref "Multi-byte_strings.md" >}}), for manual single-line
text editing. This is a overloaded variant of
[DisplayMBString]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString.md" >}}).

## Parameters

-   *int* **select** - 1 if a part of the string is selected, 0 if not.
-   *unsigned char\** **str** - Multi-byte string to display.
-   *int* **start** - The position in __bytes__ of
    the first character to display in **str**.
-   *int* **xpos** - The absolute position of the cursor in
    __bytes__. If **select**==1 it also means one
    of the ends of the selection.
-   *int* **sel_start** - The other start/end of the selection. Absolute
    position in __bytes__.
-   *int* **x** - The horizontal "homescreen" coordinate of first
    displayed character, like in
    [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}).
-   *int* **y** - The vertical coordinate of the top of the displayed
    text in __pixels__. 0px is under the header
    line.
-   *int* **pos_to_clear** - Printing stops at this position,
    effectively reducing the width of the text area. For example, if
    **`pos_to_clear`**`==5`, characters won't be displayed after the
    fifth column.
-   *int* **right_margin** __characters__ (not
    bytes!) will be cut from the right of the string, fewer characters
    will be displayed. It has a similar effect as decreasing
    **pos_to_clear** by this number.
-   *int* **key** - The exact function of this parameter is unknown. It
    must be a keycode, the syscall has different behavior in the case
    **`key`**`<30000` than `30000<=`**`key`** (In the first case the
    blinking cursor stays at its position, but in the second case, it
    disappears after a key press).

## Example

```c
DisplayMBString2(
    1,
    "(this text is not important)"
    "Hello World!"
    "-\xE5\x58(not important)",
    28, 34, 40, 5, 50, 18, 2, 30000-1
);
```

This code displays
<span style="font-size: 120%; border-style: solid; border-width: 1px;">Hello
<span style="background-color: black; color: white;">World!</span></span>
in the center of the screen.

Of course, this is an example that abuses the intended use of the
parameters of the syscall; some of the parameters are meant to be set to
the values returned when this syscall is used in conjunction with others
that actually do the text editing logic, such as
[EditMBStringCtrl2]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringCtrl2.md" >}}).
