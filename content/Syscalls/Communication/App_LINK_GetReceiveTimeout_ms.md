---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name =\nApp_LINK_GetReceiveTimeout_ms\
    \ \\| header = fxcg/serial.h \\| index =\n0x140A \\| signature = int App_LINK_GetReceiveTimeout_ms(void)\
    \ \\|\nsynopsis = Gets the timeout\u2026\u201D"
  timestamp: '2014-08-01T12:11:53Z'
title: App_LINK_GetReceiveTimeout_ms
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x140A\
**Function signature:** int App_LINK_GetReceiveTimeout_ms(void)

Gets the timeout for data reception by the built-in app Link.\

## Returns

The timeout in milliseconds for data reception by the Link app.
