---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T14:05:34Z'
title: MCSGetData1
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1563\
**Function signature:** int MCSGetData1(int offset, int len_to_copy,
void\* buffer)

Read **len_to_copy** bytes into **buffer** from the currently selected
item in main memory, starting at **offset**.\

## Parameters

-   *int* **offset** - The offset into the item to read from.
-   *int* **len_to_copy** - The amount of data you want to read.
-   *void\** **buffer** - The buffer to read into.\

## Returns

-   0 for success;
-   other values on failure.\

## Comments

Call [MCSGetDlen2]({{< ref "Syscalls/MCS/MCSGetDlen2.md" >}}) on the
item you want to read from, to select it. After doing it you can use
this syscall.

Note that between the call to MCSGetDlen2 and the call to this syscall,
process switching cannot occur, i.e., one can't call anything that
allows for entering the menu or switching to eActivity (in the case of
eActivity strips). So, make sure you do not call
[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) between the calls to
these syscalls.
