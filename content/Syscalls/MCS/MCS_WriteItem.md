---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = MCS_WriteItem \\|\nheader\
    \ = fxcg/file.h \\| index = 0x151A \\| signature = int\nMCS_WriteItem(unsigned\
    \ char\\* dir, unsigned char\\* item, short\nitemtype, int data_leng\u2026\u201D"
  timestamp: '2014-07-30T14:41:42Z'
title: MCS_WriteItem
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x151A\
**Function signature:** int MCS_WriteItem(unsigned char\* dir, unsigned
char\* item, short itemtype, int data_length, int buffer)

Creates or overwrites an item in main memory, initializing it with
**buffer**.\

## Parameters

-   **dir** - The name of the directory where the item is or will be
    located.
-   **item** - The name of the item to write.
-   **itemtype** - Used to set internal flags, including the read-only
    bit (bit 6).
-   **data_length** - Length of the data to write.
-   **buffer** - data to initialize the item with.\

## Returns

-   0 for success;
-   0x40 if **dir** does not exist;
-   0x35 if the item exists and its read-only flag (bit 6 of the first
    flags byte) is set;
-   0x32 if bit 0 of the first flags byte is set;
-   0x34 if bits 1 or 2 of the first flags byte are set;
-   0x10 if **data_length** is not 4-aligned;
-   0x11 if the MCS is full.
