---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = MCS_GetCapa \\| header\n\
    = fxcg/file.h \\| index = 0x1532 \\| signature = int MCS_GetCapa(int\\*\ncurrent_bottom)\
    \ \\| synopsis = Gets main memory capacity. \\|\nparameters\u2026\u201D"
  timestamp: '2014-07-30T13:58:03Z'
title: MCS_GetCapa
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1532\
**Function signature:** int MCS_GetCapa(int\* current_bottom)

Gets main memory capacity.\

## Parameters

-   **current_bottom** - Pointer to int that will receive the value of
    the current MCS RAM bottom.\

## Returns

Main memory capacity.
