---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = MCS_GetMainMemoryStart\n\\\
    | header = fxcg/file.h \\| index = 0x1543 \\| signature = int\nMCS_GetMainMemoryStart(void)\
    \ \\| synopsis = Gets the start address of\nthe mai\u2026\u201D"
  timestamp: '2014-07-30T14:15:40Z'
title: MCS_GetMainMemoryStart
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1543\
**Function signature:** int MCS_GetMainMemoryStart(void)

Gets the start address of the main memory.\

## Returns

The start address of the MCS.
