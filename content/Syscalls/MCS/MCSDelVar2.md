---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T13:49:10Z'
title: MCSDelVar2
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1558\
**Function signature:** int MCSDelVar2(unsigned char\* dir, unsigned
char\* item)

Deletes a variable inside a directory from the main memory.\

## Parameters

-   **dir** - The name of the directory where the file to delete is
    located;
-   **item** - The name of the item to delete.\

## Returns

-   0 for success;
-   0x40 if **dir** or the **item** in **dir** does not exist.
