---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T16:03:03Z'
title: SetAutoPowerOffTime
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x1E90\
**Function signature:** void SetAutoPowerOffTime(int durationInMinutes)

Sets the automatic power off time.\

## Parameters

-   **durationInMinutes** - amount of minutes the calculator should wait
    after the last key press, before automatically turning off. Minimum
    value is 1, there does not appear to be a maximum (other than the
    limits of `int`).
