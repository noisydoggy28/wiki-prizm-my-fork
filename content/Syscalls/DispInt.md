---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = DispInt \\| index =\n0x0259\
    \ \\| signature = void DispInt(int, int) \\| header =\nfxcg/display.h \\| parameters\
    \ = The meaning of the parameters is\nunknown. \\| synopsis\u2026\u201D"
  timestamp: '2014-07-29T19:39:10Z'
title: DispInt
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0259\
**Function signature:** void DispInt(int, int)

The exact function of this syscall is unknown.\

## Parameters

The meaning of the parameters is unknown.
