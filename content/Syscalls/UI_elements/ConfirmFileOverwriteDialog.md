---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:02:51Z'
title: ConfirmFileOverwriteDialog
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1802\
**Function signature:** int ConfirmFileOverwriteDialog(unsigned short\*
filename)

Shows a file overwrite confirmation dialog for the specified filename.\

## Parameters

-   **filename** - 16 bit string that will be shown as the file name to
    overwrite.\

## Returns

1 if F1 was pressed (Overwrite OK), 0 if F6, EXIT, or QUIT are pressed (Overwrite not OK).
