---
revisions:
- author: Ahelper
  comment: libfxcg API
  timestamp: '2014-11-23T00:28:26Z'
title: Scrollbar
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1A0A\
**Function signature:** void Scrollbar(struct scrollbar\* scrollbar)

Displays a vertical scrollbar that cannot reach the status area at the
top.\

## Parameters

-   *TScrollbar\** **scrollbar** - settings for the scrollbar (indicator
    and bar position and size).

    ```
    typedef struct{
      unsigned int i1; // unknown, set to zero
      unsigned int indicatormaximum; // maximum logical indicator range
      unsigned int indicatorheight; // height of the indicator in units of indicatormaximum
      unsigned int indicatorpos; // indicator position in units of indicatormaxiumum
      unsigned int i5; // unknown, set to zero
      unsigned short barleft; // left position of the bar in pixels
      unsigned short bartop; // top position of the bar in pixels
      unsigned short barheight; // height of the bar in pixels
      unsigned short barwidth; // width of the bar in pixels
    } TScrollbar;
    ```