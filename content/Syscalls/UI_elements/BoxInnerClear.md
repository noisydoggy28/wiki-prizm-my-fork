---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:02:09Z'
title: BoxInnerClear
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x17FD\
**Function signature:** void BoxInnerClear(int)

The function of this syscall is unknown; it seems to be related to
message boxes, being probably involved in the drawing of a message box.\

## Parameters

The meaning of the parameter is unknown.
