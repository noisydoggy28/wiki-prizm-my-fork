---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:04:19Z'
title: DrawHeaderLine
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x02BB\
**Function signature:** void DrawHeaderLine(void);

The function of this syscall is unconfirmed, but apparently all it does
is draw to VRAM the small line separating the status area from the rest
of the screen contents.
