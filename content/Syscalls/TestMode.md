---
revisions:
- author: Gbl08ma
  timestamp: '2015-03-16T18:19:16Z'
title: TestMode
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x0EA7\
**Function signature:** void TestMode(int vram)

Opens an hidden OS menu, the Test Mode, where some OS functionality can
be tested and a look into the OS internals can be taken.\

## Parameters

-   **vram** - Whether to restore the previous VRAM when exiting the
    Test Mode.

## Functionality

Functionality includes:

-   [MCS]({{< ref "File_System.md" >}}) browser ("Valiable Manager"
    (*sic*));
-   Memory viewer (but not editor) (F4 and F5 in the "Valiable
    Manager");
-   [Font]({{< ref "Fonts.md" >}}) and character code checking,
    including for "GB codes" (Chinese codes plus alternatives to some
    Latin ones), probably a reference to the GB 18030 character set;
-   [Setup]({{< ref "Setup.md" >}}) visualizer (opens a menu like the
    ones seen when pressing Shift+Menu in different built-in apps, but
    showing all possible settings);
-   [Function key bitmap]({{< ref "Syscalls/Locale/GetFKeyPtr.md" >}})
    visualizer;
-   Ability to set function key label color and screen frame color;
-   [Fugue FAT File System]({{< ref "File_System.md" >}}) and
    [Bfile]({{< ref "Syscalls/Bfile/" >}}) testing functions;
-   Memory usage visualizer for the [heap]({{< ref
    "/OS_Information/Processes.md#heap" >}}), the
    main process [stack]({{< ref "/OS_Information/Processes.md#stack" >}}) and the child stack;
-   Disabled function to dump flash data over USB.

Some of this functionality is present and extended in Simon Lothar's
Insight add-in.

## How to access {#how_to_access}

Besides calling the corresponding syscall (something the Insight add-in
lets one do), there's also the key combination used for entering the
diagnostics mode: on the "Delete all data" screen (which doesn't delete
anything), one can quickly type 5963 to open the Test Mode. So the key
combination becomes as follows:

\[OPTN\]+\[EXP\]+\[AC/ON\], then quickly type 5963.

More key combinations can be found
[here]({{< ref "Secret_Key_Combinations.md" >}}).
