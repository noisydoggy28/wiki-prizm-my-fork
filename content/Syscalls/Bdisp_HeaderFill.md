---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T19:02:14Z'
title: Bdisp_HeaderFill
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D86\
**Function signature:** void Bdisp_HeaderFill(unsigned char c_idx1,
unsigned char c_idx2)

Fills the status area with a checkerboard background (seen when inside
an eActivity strip) with the given colors.

## Parameters

-   *unsigned char* **c_idx1** - first color
-   *unsigned char* **c_idx2** - second color

## Color table {#color_table}

| color index | hex              |
|-------------|------------------|
| 0           | 0x0000 (black)   |
| 1           | 0x001F (blue)    |
| 2           | 0x07E0 (green)   |
| 3           | 0x07FF (cyan)    |
| 4           | 0xF800 (red)     |
| 5           | 0xF81F (magenta) |
| 6           | 0xFFE0 (yellow)  |
| 7           | 0xFFFF (white)   |
| 8           | 0x0008 (marine)  |
| 9           | 0x0009           |
| A           | 0x000A           |
| B           | 0x000B           |
| C           | 0x000C           |
| D           | 0x000D           |
| E           | 0x000E           |
| F           | 0xFFDF (silver)  |
|             |                  |
