---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T10:49:52Z'
title: Bdisp_AllClr_VRAM
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0272\
**Function signature:** void Bdisp_AllClr_VRAM(void)

Clears the contents of VRAM with white pixels.\

## Comments

It's not suggested you use this syscall to clear VRAM if speed is
important. Instead do 32-bit writes as (partially) described in
[Display]({{< ref "Technical_Documentation/Display.md" >}}). If you will
be redrawing the entire display anyway, there is no need to clear VRAM
in any case.
