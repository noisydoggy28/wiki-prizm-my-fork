---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:19:00Z'
title: Keyboard_PutKeycode
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x12C6\
**Function signature:** int Keyboard_PutKeycode(int x, int y, int
keycode)

Injects a key code into a key buffer.\

## Parameters

If both **x** and **y** are below zero, **keycode** is injected into the
[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) buffer and its next
call will return **keycode**. This works even on the "fx-CG10/20
Manager" emulator.

If **x** and **y** are equal or higher than zero, they will be treated
as matrix codes and injected into different lower level key buffer(s),
used by lower-level keyboard reading syscalls, and **keycode** will be
ignored.\

## Comments

Injection of matrix codes only works when a keyboard reading method such
as [GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) is already
waiting, and so it has to be performed from a
[timer]({{< ref "/OS_Information/Timers.md" >}}) handler which must be started before
GetKey is called.
