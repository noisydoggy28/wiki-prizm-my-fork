---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:20:23Z'
title: Set_FKeys2
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x0129\
**Function signature:** void Set_FKeys2(unsigned int p1)

The function of this syscall is not yet known in detail. Under normal
circumstances it shouldn't be invoked.\

## Parameters

The meaning of the parameter is not yet known.\

## Comments

This syscall claims some kind of internal handle, which is a limited
resource. It is not clear yet, how to properly release the handle, or
even if it can be released at all. Simon Lothar's document indicates
that calling it is only necessary if an add-in-prototype is run from
RAM.
