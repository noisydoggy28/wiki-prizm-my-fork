---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:17:36Z'
title: Bkey_SetAllFlags
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x0EA1\
**Function signature:** void Bkey_SetAllFlags(short flags)

Sets flags corresponding to keyboard input.\

## Parameters

-   **flags** - the short value at 0xFD80161C will be set to this
    parameter's value.\

## Comments

For a description of known flags, see
[Bkey_GetAllFlags]({{< ref "Syscalls/Keyboard/Bkey_GetAllFlags.md" >}}).
