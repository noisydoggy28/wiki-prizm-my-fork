---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:17:25Z'
title: Bkey_ClrAllFlags
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x0111\
**Function signature:** void Bkey_ClrAllFlags(void)

Clears all of the flags in a set related to keyboard input.\

## Comments

Clears all bits of the `short` value at 0xFD80161A. From Simon Lothar's
documentation:

    Bit 0x0020: see 0x8013FF2C
    Bit 0x0040: see 0x8024CF3C
    Bit 0x0080: see 0x80249570
    Bit 0x0200: see 0x8014BAB2 (f. i. used to flag the OverwriteConfimation-Dialog(0x0D91))
    Bit 0x1000: see 0x8013FBDA
    Bit 0x8000: is set during showing a battery-low-status message (f. i. syscall 0x1E75).

    The FKey_mapping (0x0129/0x012B) is bypassed, if any of 0xFD80161A's bits are set.
