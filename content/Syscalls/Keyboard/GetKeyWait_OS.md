---
revisions:
- author: Gbl08ma
  timestamp: '2015-02-11T16:24:10Z'
title: GetKeyWait_OS
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x12BF\
**Function signature:** int GetKeyWait_OS(int\* column, int\* row, int
type_of_waiting, int timeout_period, int menu, unsigned short\* keycode)

Reads the keyboard input and returns the result as matrix values (row
and column). The function can be made to block or not, supports setting
a timeout, and can optionally handle the MENU key for opening the Main
Menu.\

## Parameters

-   **column** - pointer to integer that will receive the column number
    of the next key in the keyboard buffer.
-   **row** - pointer to integer that will receive the row number of the
    next key in the keyboard buffer.
-   **type_of_waiting** - although not yet specified in libfxcg, the
    values of this parameter are usually mentioned by a name:
    -   0 - *KEYWAIT_HALTON_TIMEROFF*: if there are no characters in the
        key buffer, this function waits until a character arrives and
        then returns immediately (blocking operation mode);
    -   1 - *KEYWAIT_HALTOFF_TIMEROFF*: the function returns immediately
        (non-blocking operation mode);
    -   2 - *KEYWAIT_HALTON_TIMERON*: if no is pressed within the time
        specified by the **timeout_period** parameter, this function
        times out.
-   **timeout_period** - timeout period, in seconds, for the
    *KEYWAIT_HALTON_TIMERON* mode. The timeout period can be between 0
    and 3600 seconds.
-   **menu** - if zero and **type_of_waiting** is not
    *KEYWAIT_HALTOFF_TIMEROFF*, the MENU key is handled, opening the
    Main Menu. If not zero or if **type_of_waiting** is
    *KEYWAIT_HALTOFF_TIMEROFF*, the MENU key is handled like any other
    key and the Main Menu is not opened.
-   **keycode** - pointer to `unsigned short` that receives a keycode if
    one has been set beforehand with
    [Keyboard_PutKeycode]({{< ref "Syscalls/Keyboard/Keyboard_PutKeycode.md" >}}).
    Does not appear to have any other use.\

## Returns

The values returned are usually mentioned as follows:

-   0 - *KEYREP_NOEVENT*: no key available (returned only when
    **type_of_waiting** is *KEYWAIT_HALTOFF_TIMEROFF*);
-   1 - *KEYREP_KEYEVENT*: key available;
-   2 - *KEYREP_TIMEREVENT*: timeout (returned only when
    **type_of_waiting** is *KEYWAIT_HALTON_TIMERON*).

## Key codes {#key_codes}

The matrix codes returned start at 0x0101, unlike the matrix codes
returned by most syscalls, which start at 0x0000.

The column number goes from 7 to 2, from left to right.

The row number goes from 2 to 10, from bottom to top

Matrix code examples (0xCCRR, CC=column, RR=row):

-   F1: 0x070A
-   F6: 0x020A
-   EXE: 0x0302
-   0: 0x0702

The AC/on key is an exception, its row is 1 as well as its column:
0x0101.

For a full key code map, refer to the [keycode
reference]({{< ref "Keycode_Reference.md" >}}) or the Insight add-in by
Simon Lothar.
