---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = NibbleToHex \\| header\n\
    = fxcg/misc.h \\| index = 0x1346 \\| signature = void\nNibbleToHex(unsigned char\
    \ value, unsigned char\\* result) \\| synopsis\n= Converts a nibbl\u2026\u201D"
  timestamp: '2014-07-31T17:41:40Z'
title: NibbleToHex
---

## Synopsis

**Header:** fxcg/misc.h\
**Syscall index:** 0x1346\
**Function signature:** void NibbleToHex(unsigned char value, unsigned
char\* result)

Converts a nibble to its hexadecimal representation.\

## Parameters

-   **value** - the value to convert.
-   **result** - pointer to string that will hold the conversion result.
