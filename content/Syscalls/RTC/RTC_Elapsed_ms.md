---
revisions:
- author: Gbl08ma
  timestamp: '2015-02-10T22:59:49Z'
title: RTC_Elapsed_ms
---

## Synopsis

**Header:** fxcg/rtc.h\
**Syscall index:** 0x02C2\
**Function signature:** int RTC_Elapsed_ms(int start_value, int
duration_in_ms)

Checks whether a given amount of milliseconds has elapsed since a
specified [ticks]({{< ref "Syscalls/RTC/RTC_GetTicks.md" >}}) count.\

## Parameters

-   **start_value** - ticks count to compare the current time against.
-   **duration_in_ms** - period in milliseconds one wishes to know
    whether has passed since **start_value** was collected.\

## Returns

1 if duration_in_ms has elapsed, 0 if not.\

## Comments

One needs to take care and not use this syscall when **start_value** has
been collected before midnight, and the current time is past midnight,
because the [ticks]({{< ref "Syscalls/RTC/RTC_GetTicks.md" >}}) count
resets at midnight. If such a situation happens, a almost full day will
pass before this syscall returns 1, or it may even never return 1, in
the case that the amount of ticks to midnight is lower than the amount
of ticks corresponding to **duration_in_ms**.
