---
revisions:
- author: Gbl08ma
  timestamp: '2015-02-10T23:00:04Z'
title: RTC_GetTicks
---

## Synopsis

**Header:** fxcg/rtc.h\
**Syscall index:** 0x2C1\
**Function signature:** int RTC_GetTicks(void)

Gets the number of ticks elapsed since midnight. The RTC ticks at 128
Hz.\

## Returns

The number of ticks since midnight, resetting every day. A tick occurs
every 1/128, approximately 0.00781, second.
