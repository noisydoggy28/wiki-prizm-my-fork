---
revisions:
- author: Tari
  timestamp: '2012-05-17T04:00:32Z'
title: Bdisp_SetBacklightLevel
---

## Synopsis

**Header:** Not currently implemented in libfxcg\
**Syscall index:** 0x0199\
**Function signature:** void Bdisp_SetBacklightLevel(char level)

Sets the display backlight level.

## Parameters

-   **level**: The display brightness to set, between 1 and 5 inclusive.
    Presumably 5 is brightest, can someone confirm?

## Comments

This routine has not yet been added to libfxcg.
