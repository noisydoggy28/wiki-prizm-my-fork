---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = sys_free \\| header =\nfxcg/heap.h\
    \ \\| index = 0x1F42 \\| signature = void sys_free(void \\*p)\n\\| synopsis =\
    \ Frees space allocated on the heap with\n\\[\\[sys_malloc\\]\\]. \\|\u2026\u201D"
  timestamp: '2014-07-30T19:57:09Z'
title: Sys_free
---

## Synopsis

**Header:** fxcg/heap.h\
**Syscall index:** 0x1F42\
**Function signature:** void sys_free(void \*p)

Frees space allocated on the heap with
[sys_malloc]({{< ref "Syscalls/Heap/sys_malloc.md" >}}).\

## Parameters

-   **p** - pointer to block of heap memory to free.\

## Comments

The heap implementation on the Prizm appears to have some bugs; it also
appears to not be able to optimize heap allocation so that fragmentation
is avoided. See [this forum
thread](http://www.cemetech.net/forum/viewtopic.php?t=7539) for more
information and discussion.

The [stack]({{< ref "/OS_Information/Processes.md#stack" >}}) can provide a bigger continuous RAM
area than the heap (up to about 500 KiB, depending on static RAM
requirements, versus about 128 KiB). This, plus the limitations
described above, means that to get the most out of the memory available
to add-ins, one has to use non-standard (*"incorrect"*) practices such
as preferably using the stack instead of the heap.
