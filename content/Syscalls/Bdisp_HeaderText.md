---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T19:04:43Z'
title: Bdisp_HeaderText
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D82\
**Function signature:** void Bdisp_HeaderText(void)

Draws the message set by [DefineStatusMessage]({{< ref "Syscalls/DefineStatusMessage.md" >}}) to VRAM.\
This is run by [DisplayStatusArea]({{< ref "Syscalls/DisplayStatusArea.md" >}}) if the appropriate flag is enabled by [DefineStatusAreaFlags]({{< ref "Syscalls/DefineStatusAreaFlags.md" >}}).
