---
revisions:
- author: Gbl08ma
  timestamp: '2015-02-11T12:25:45Z'
title: CMT_Delay_100micros
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x11D7\
**Function signature:** void CMT_Delay_100micros(int
100microseconds_delay)

Holds program execution by a given amount of 0.1 millisecond, or 100
microsecond, units.\

## Parameters

-   **100microseconds_delay** - delay during which to hold program
    execution, in units of 100 microseconds.\

## Comments

According to Simon Lothar's documentation, this syscall works based on
the 7730 CMT.
