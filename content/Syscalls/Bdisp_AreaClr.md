---
revisions:
- author: JosJuice
  timestamp: '2014-08-31T07:50:29Z'
title: Bdisp_AreaClr
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x02B2\
**Function signature:** `void Bdisp_AreaClr(TBdispFillArea* area, unsigned char target, unsigned short color)`

Performs a VRAM or DD operation to fill an area with a color, depending
on the mode of **area**.

## Parameters

area
: area to fill, and mode

  ```
  struct TBdispFillArea {
      int x1;
      int y1;
      int x2;
      int y2;
      unsigned char mode;
  };
  ```

  mode
  : if 0, area is filled white; if 1, area is
  filled with **color**, if 2, area is shaded with **color**; if 4,
  area will be inverted (may be a bug).

target
: where to perform the operation, VRAM or
DD. If bit target.0 is set, the function performs a VRAM operation.
If bit target.1 is set, the function performs a DD operation.

color
: color that will be used to fill when certain modes are used

