---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T16:28:43Z'
title: PrintMiniGlyph
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x023B\
**Function signature:** void PrintMiniGlyph(int x, int y, void\*glyph,
int mode_flags, int glyph_width, int, int, int, int, int color, int
back_color, int )

Draws a glyph in the same small font used by
[PrintMini]({{< ref "Syscalls/PrintMini.md" >}}).\

## Parameters

-   *int* **x** - Position of the text on a horizontal axis, in pixels
-   *int* **y** - Position of the text on a vertical axis, in pixels
-   *void\** **glyph** - Pointer to the glyph to draw, can be obtained
    using [GetMiniGlyphPtr]({{< ref "Syscalls/GetMiniGlyphPtr.md" >}})
-   *int* **mode_flags** - 0x40 allows for drawing text on the status
    area; 0x04 inverts **text_color** and **back_color**; 0x02 does not
    use **back_color**
-   *int* **glyph_width** - number of bits to be used of the
    mini-glyph-array shorts (from left), can be obtained using
    [GetMiniGlyphPtr]({{< ref "Syscalls/GetMiniGlyphPtr.md" >}})
-   *int* **color** - The color of the glyph
-   *int* **back_color** - The color of the glyph background. Depends on
    **mode_flags**

The meaning of the other parameters is still unknown, set them to zero.
