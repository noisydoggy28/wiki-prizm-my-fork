---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  timestamp: '2014-12-08T11:47:35Z'
title: Timers
---

This is a list of [syscalls]({{< ref "syscalls.md" >}}) that allow for
controlling [timers]({{< ref "/OS_Information/Timers.md" >}}).

Timer handlers must not call any direct-display (DD) syscall.  [Bfile]({{< ref
"Syscalls/Bfile/" >}}) syscalls may cause problems when timers are installed,
see [Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref
"Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}}).  Make sure to
uninstall the timers you don't want to run, and not just stop them. See
[Outstanding issues]({{< ref "/OS_Information/Timers.md#outstanding_issues" >}})
for more information.
