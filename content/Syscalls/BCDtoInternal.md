---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-31T18:35:36Z'
title: BCDtoInternal
---

## Synopsis

**Header:** fxcg/misc.h\
**Syscall index:** 0x0160\
**Function signature:** int BCDtoInternal(struct BCDinternal\* target,
struct BCDvalue\* source)

Converts a BCD-coded digit to its special internal representation. Helps
saving code when converting a BCD value to a `double`.\

## Parameters

target
: Pointer to a struct that will hold the conversion result. The struct is as follows:

  ```c
  struct BCDinternal {
    int exponent;
    int sign;
    int unknown;
    char mantissa[15];
  }
  ```

-   **source** - pointer to BCD value to convert. The struct containing
    it is as follows:
    
    ```c
    typedef struct {
      unsigned char hnibble:4;
      unsigned char lnibble:4;
    } BCDbyte;

    struct BCDvalue {
      unsigned short exponent:12;
      unsigned short mantissa0:4;
      BCDbyte mantissa[7];
      char flags;
      short info;
    };
    ```

## Returns

0 on success, -1 on error.
