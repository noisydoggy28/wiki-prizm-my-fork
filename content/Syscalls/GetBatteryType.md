---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = GetBatteryType \\|\nheader\
    \ = fxcg/system.h \\| index = 0x12D5 \\| signature = int\nGetBatteryType(void)\
    \ \\| synopsis = Gets the battery type used for\nestimating the b\u2026\u201D"
  timestamp: '2014-08-01T16:41:56Z'
title: GetBatteryType
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x12D5\
**Function signature:** int GetBatteryType(void)

Gets the battery type used for estimating the battery level. This is the
setting adjusted on start-up, or when one goes into System-\>F6-\>F1.\

## Returns

1 for Alkaline batteries, 2 for Ni-MH batteries. On the "fx-CG10/20
Manager" emulator, this setting is undefined, and set to 0.
