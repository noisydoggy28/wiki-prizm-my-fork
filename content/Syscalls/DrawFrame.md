---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T10:37:26Z'
title: DrawFrame
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x02A8\
**Function signature:** void DrawFrame(int color)

Changes the color of the screen border.\

## Parameters

-   **color** - The
    [color]({{< ref "Technical_Documentation/Display.md" >}}) to draw
    the border with.\

## Comments

Directly changes the color of the screen border, does not involve VRAM
(so [Bdisp_PutDisp_DD]({{< ref "Syscalls/Bdisp_PutDisp_DD.md" >}}) need
not follow). See
[Display]({{< ref "Technical_Documentation/Display.md" >}}) for more
information.
