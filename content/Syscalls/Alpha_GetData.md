---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-03T22:42:50Z'
title: Alpha_GetData
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x0034\
**Function signature:** void Alpha_GetData(char VarName, struct
BCDvalue\* Dest)

Gets the value of a Alpha variable, which can be used in most expression
evaluators throughout the OS, as well as BASIC programs.\

## Parameters

-   **VarName** - name of the variable to retrieve.
-   **Dest** - pointer to a struct used for number representation by the
    OS math library, and that is documented at
    [BCDtoInternal]({{< ref "Syscalls/BCDtoInternal.md" >}}) (the
    syscall used in aiding the conversion from a `double` to a
    BCDvalue).
