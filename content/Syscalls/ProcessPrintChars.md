---
title: ProcessPrintChars
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1300\
**Function signature:** int ProcessPrintChars(unsigned short code);

See [multi-byte strings]({{% ref "Multi-byte_strings.md#cjk-text" %}})
for details on text encoding.

### Parameters

`code`
: A character set code specifying what character set the following
  text is encoded in. Currently only two codes are known to be
  supported:

  0x0000
  : default, Latin character set. `PRINT_CHARSET_DEFAULT` in libfxcg.
  
  0x03A8
  : GB 18030, Chinese character set. `PRINT_CHARSET_GB18030` in libfxcg.

### Returns

1 if the provided character set code is recognized and some action was
taken, or 0 otherwise.
