---
revisions:
- author: Gbl08ma
  timestamp: '2015-02-15T13:30:27Z'
title: Locate_OS
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1863\
**Function signature:** int locate_OS(int x, int y)

Sets the cursor position for
[Print_OS]({{< ref "Syscalls/Print_OS.md" >}}).\

## Parameters

-   **x** Must be in range of \[1,21\]
-   **y** Must be in range of \[1,8\]\

## Returns

0 on failure, 1 on success.\

## Comments

This function does bounds checking; if **x** or **y** are out of range,
it does nothing, otherwise, it calls
[Cursor_SetPosition]({{< ref "Syscalls/Cursor_SetPosition.md" >}}). As
the latter function also does bounds checking, it is best to use it
directly in order to avoid duplicate checks, achieving better
performance.


    locate_OS:
        mov     #1, r2
        cmp/ge  r2, r4          ! Is r4 >= 1? If so set T=1
        bf      locate_OS_exit  ! If R4 is less than one do not set the cursor position
        mov     #21, r6
        cmp/gt  r6, r4
        bt      locate_OS_exit  ! Don't set the cursor position if x>21
        cmp/ge  r2, r5
        bf      locate_OS_exit  ! Don't set the cursor position if y<1
        mov     #8, r2
        cmp/gt  r2, r5
        bt      locate_OS_exit  ! Don't set the cursor position if y>8
        mov.l   #Cursor_SetPosition, r2
        add     #-1, r5
        jmp     @r2 ! Cursor_SetPosition
        add     #-1, r4
    ! ---------------------------------------------------------------------------

    locate_OS_exit:
        rts
        nop
    ! End of function locate_OS
