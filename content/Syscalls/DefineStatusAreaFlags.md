---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = DefineStatusAreaFlags\n\\\
    | index = 0x02B8 \\| signature = int DefineStatusAreaFlags(int P1,\nint flags,\
    \ void\\* P3, void\\* P4) \\| header = fxcg/display.h \\|\nparameters\u2026\u201D"
  timestamp: '2014-07-29T18:27:17Z'
title: DefineStatusAreaFlags
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x02B8\
**Function signature:** int DefineStatusAreaFlags(int mode, int flags,
char\* color1, char\* color2)

Defines what items are to be shown on the status area, and sets the colors
used to shade the header.\
When DisplayStatusArea is run, the status area will be filled with a
checkerboard pattern of two colors. These two colors can be set by this function.

## Parameters

-   **mode** - If 0, the header will be shaded with white, and the status icon flags
    will be set to 0.\
    If 1, the header will be shaded with white, and the status icon flags will
    be set to 0x303 (all flags).\
    If 2, the two currently set header colors will be returned in **\*color1**
    and **\*color2**, if these are not null.\
    If 3, the header colors will be set to **\*color1** and **\*color2**, if these
    are not null. The status icon flags will be set to **flags**.\
    If 4, the status icon flags will be set to the result of (current flags \|
    **flags**) (OR operation - this results in setting any previous flags plus
    the ones set in **flags**).\
    If 5, the status icon flags will set to be the result of \~(current flags
    & **flags**) (NAND operation - this results in setting all the previous
    flags minus the ones set in **flags**).
-   **flags** - status area flags to operate with, when **mode** is 3, 4
    or 5. See the end of this page for a table of valid flags.
-   **color1** and **color2** are pointers to chars that will be copied to the
    system variables or will receive their contents, depending on the value
    of **mode**. See [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}) for a list of colors.

## Example

The following code will set the following items to appear on the
status bar: the battery meter, text (to be set with
[DefineStatusMessage]({{< ref "Syscalls/DefineStatusMessage.md" >}})),
custom glyphs (to be set with
[DefineStatusGlyph]({{< ref "Syscalls/DefineStatusGlyph.md" >}})) and
the keyboard input method (shown to the right of the battery icon when
any Alpha, Clip or Shift mode is enabled). It will also cause the status area
to be filled with a checkerboard of green and blue.

```
char color1 = TEXT_COLOR_BLUE;
char color2 = TEXT_COLOR_GREEN;
DefineStatusAreaFlags(3, SAF_BATTERY | SAF_TEXT | SAF_GLYPH | SAF_ALPHA_SHIFT, &color1, &color2);
```

## Appendix

Table of status bar flags:

| define                 | value  | meaning                                                                                          |
|------------------------|--------|--------------------------------------------------------------------------------------------------|
| SAF_BATTERY            | 0x0001 | Battery icon                                                                                     |
| SAF_ALPHA_SHIFT        | 0x0002 | Alpha/Shift/Clip keyboard modifier indicator                                                     |
| SAF_SETUP_INPUT_OUTPUT | 0x0004 | Input/Output mode (Math or Line)                                                                 |
| SAF_SETUP_FRAC_RESULT  | 0x0008 | Fraction display mode                                                                            |
| SAF_SETUP_ANGLE        | 0x0010 | Angle (trigonometry) mode                                                                        |
| SAF_SETUP_COMPLEX_MODE | 0x0020 | Complex number mode (Real, a+bi, r\<O)                                                           |
| SAF_SETUP_DISPLAY      | 0x0040 | Number display mode (Norm1, Norm2, Fix, Sci...)                                                  |
| SAF_TEXT               | 0x0100 | Custom text, to be set with [DefineStatusMessage]({{< ref "Syscalls/DefineStatusMessage.md" >}}) |
| SAF_GLYPH              | 0x0200 | Custom glyph, to be set with [DefineStatusGlyph]({{< ref "Syscalls/DefineStatusGlyph.md" >}})    |
