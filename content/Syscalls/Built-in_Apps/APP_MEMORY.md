---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:26:43Z'
title: APP_MEMORY
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1632\
**Function signature:** void APP_MEMORY(void);

Opens the built-in Memory app.\

## Comments

Note that even though most built-in apps do not return, the calling code
is kept on stack and one can return to it using a ugly hack, for example
through [timers]({{< ref "/OS_Information/Timers.md" >}}), setjmp and longjmp. The
reason why they don't return is that they expect to use [GetKey as an
exit point]({{< ref "Syscalls/Keyboard/GetKey.md" >}}).
