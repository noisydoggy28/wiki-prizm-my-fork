---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = KeyboardIcon \\| index\n\
    = 0x1D8A \\| signature = void KeyboardIcon(unsigned int) \\| header =\nfxcg/display.h\
    \ \\| parameters = The meaning of the parameter is\nunknown\u2026\u201D"
  timestamp: '2014-07-29T19:20:24Z'
title: KeyboardIcon
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D8A\
**Function signature:** void KeyboardIcon(unsigned int)

The exact function of this syscall is unknown; it is probably related to
status area items.\

## Parameters

The meaning of the parameter is unknown.\

## Comments

Instead of trying to control individual status area items directly,
using [status area
flags]({{< ref "Syscalls/DefineStatusAreaFlags.md" >}}) may be more
appropriate.
