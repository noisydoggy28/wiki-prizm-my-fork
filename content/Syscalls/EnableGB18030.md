---
title: EnableGB18030
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x01E0\
**Function signature:** void EnableGB18030();

Sets following text display syscalls to use GB 18030 text encoding.
See [multi-byte strings]({{% ref "Multi-byte_strings.md#cjk-text" %}})
for details.