---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Bdisp_ShapeBase3XVRAM\n\\\
    | index = 0x01BE \\| signature = void Bdisp_ShapeBase3XVRAM(void\\*\nshape) \\\
    | header = fxcg/display.h \\| parameters = \\* \u2019\u2018\u2019shape\u2019\u2019\
    \u2019 - a\npoin\u2026\u201D"
  timestamp: '2014-07-29T11:46:13Z'
title: Bdisp_ShapeBase3XVRAM
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x01BE\
**Function signature:** void Bdisp_ShapeBase3XVRAM(void\* shape)

Draws a shape to VRAM in 3x3 pixels legacy mode, allowing the
development of a compatibility layer for fx9860G code.\

## Parameters

-   **shape** - a pointer to a (currently unknown) struct specifying the
    shape.
