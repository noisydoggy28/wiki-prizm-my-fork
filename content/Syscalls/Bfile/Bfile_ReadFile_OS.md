---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T10:52:16Z'
title: Bfile_ReadFile_OS
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DAC\
**Function signature:** int Bfile_ReadFile_OS(int handle, void \*buf,
int size, int pos)

Reads bytes from an open file.\

## Parameters

-   **handle**: File handle as returned by
    [Bfile_OpenFile_OS]({{< ref "Syscalls/Bfile/Bfile_OpenFile_OS.md" >}}).
-   **buf**: Location to store data read from file.
-   **size**: Number of bytes to read.
-   **pos**: File offset to read at, or -1 to read from the current file
    pointer (like stdio).\

## Returns

Number of bytes read, or a negative error code on failure.\

## Comments

Large values for *size* (one megabyte or more) may cause issues (hang
the calculator).

If **size** is \>0, then the position in the file handle will be moved
to **pos**+**size** after the function. If **size** is 0, the position
will not be set to **pos**. Use
[Bfile_SeekFile_OS]({{< ref "Syscalls/Bfile/Bfile_SeekFile_OS.md" >}})
to set the file pointer.

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.
