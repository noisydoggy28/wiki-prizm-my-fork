---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T10:57:03Z'
title: Bfile_SeekFile_OS
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DA9\
**Function signature:** int Bfile_SeekFile_OS(int handle, int pos)

Changes the position in the file handle, similar to fseek.\

## Parameters

-   **handle**: File handle as returned by
    [Bfile_OpenFile_OS]({{< ref "Syscalls/Bfile/Bfile_OpenFile_OS.md" >}}).
-   **pos**: Position (absolute, not relative) to seek to.\

## Returns

Returns the new position in the file handle.\

## Comments

This function seeks to an absolute location in the file pointer. To seek
backwards, use
[Bfile_GetFileSize_OS]({{< ref "Syscalls/Bfile/Bfile_GetFileSize_OS.md" >}})
and add the offset to that.

**WARNING**: This function resizes the file if **pos** \>
[Bfile_GetFileSize_OS]({{< ref "Syscalls/Bfile/Bfile_GetFileSize_OS.md" >}}).
The file cannot shrink, only expand. If you need to shrink a file,
[delete]({{< ref "Syscalls/Bfile/Bfile_DeleteEntry.md" >}}) then
[recreate]({{< ref "Syscalls/Bfile/Bfile_CreateEntry_OS.md" >}}) it, and
[write]({{< ref "Syscalls/Bfile/Bfile_WriteFile_OS.md" >}}) the new
contents.

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.
