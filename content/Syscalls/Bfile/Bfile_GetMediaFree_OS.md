---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T12:59:28Z'
title: Bfile_GetMediaFree_OS
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DA5\
**Function signature:** int Bfile_GetMediaFree_OS(unsigned short\*
media_id, int\* freespace)

Gets the amount of free space on a device (the storage memory).\

## Parameters

-   *unsigned short\** **media_id** - Pointer to a 16 bit string with
    the device identifier (\\\\fls0).
-   *int\** **freespace** - Pointer to *int* that will hold the amount
    of free bytes on the device.\

## Returns

0 on success or a negative error code on failure.\

## Comments

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.\

## Example

Here's how to get the amount of free bytes on the storage memory:


    int smemfree;
    unsigned short smemMedia[10]={'\\','\\','f','l','s','0',0};
    Bfile_GetMediaFree_OS( smemMedia, &smemfree );
    // the free space in bytes on the storage memory is now on smemfree.
