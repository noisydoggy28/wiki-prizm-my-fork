---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = SetCursorFlashToggle\n\\\
    | index = 0x08D2 \\| signature = int SetCursorFlashToggle(int) \\|\nheader = fxcg/display.h\
    \ \\| synopsis = The function of this syscall\nis not\u2026\u201D"
  timestamp: '2014-07-29T15:03:54Z'
title: SetCursorFlashToggle
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x08D2\
**Function signature:** int SetCursorFlashToggle(int)

The function of this syscall is not yet known in detail.\

## Comments

Probably enables or disables the cursor flash according to the
parameter, and returns the current state. Possibly related to
[Cursor_SetFlashOn]({{< ref "Syscalls/Cursor_SetFlashOn.md" >}}),
[Cursor_SetFlashOff]({{< ref "Syscalls/Cursor_SetFlashOff.md" >}}) and
[Keyboard_CursorFlash]({{< ref "Syscalls/Keyboard_CursorFlash.md" >}}).
