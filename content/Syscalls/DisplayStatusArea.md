---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = DisplayStatusArea \\|\nindex\
    \ = 0x1D81 \\| signature = void DisplayStatusArea(void); \\|\nheader = fxcg/display.h\
    \ \\| synopsis = Manually makes the status area\nbe draw\u2026\u201D"
  timestamp: '2014-07-29T18:53:57Z'
title: DisplayStatusArea
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D81\
**Function signature:** void DisplayStatusArea(void);

Manually makes the status area be drawn to VRAM. Has no effect if the
status area is disabled with
[EnableStatusArea]({{< ref "Syscalls/EnableStatusArea.md" >}}).\

## Comments

This syscall seems to be automatically called by
[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}), at least in some
contexts.
