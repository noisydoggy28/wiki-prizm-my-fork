---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T10:20:44Z'
title: Serial_Open
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BB7\
**Function signature:** int Serial_Open(unsigned char\* mode)

Opens the 3-pin serial port with the specified parameters.\

## Parameters

-   **mode** - pointer to a six-byte array containing the settings for
    the serial port:

| Index | Description | Values                                    |
|-------|-------------|-------------------------------------------|
| 0     | Unknown     | Always 0                                  |
| 1     | Bit rate    | See [bitrate settings](#bitrate-settings) |
| 2     | Parity      | See [parity settings](#parity-settings)   |
| 3     | Data length | 0 = 8-bit, 1 = 7-bit                      |
| 4     | Stop bits   | 0 = 1 stop bit, 1 = 2 stop bits           |
| 5     | Unused      | Always 0                                  |

### Bitrate settings

| Value | Bit rate |
|-------|----------|
| 0     | 300      |
| 1     | 600      |
| 2     | 1200     |
| 3     | 2400     |
| 4     | 4800     |
| 5     | 9600     |
| 6     | 19200    |
| 7     | 38400    |
| 8     | 57600    |
| 9     | 115200   |

### Parity settings

| Value | Parity |
|-------|--------|
| 0     | None   |
| 1     | Odd    |
| 2     | Even   |

## Returns

-   0 if successful;
-   3 if the serial port is already open;
-   4 if **mode**\[0\] is not zero.\

## Example

Check if the serial port is already open, and configure it for 9600 bps
8N1 operation if not.

    if (Serial_IsOpen() != 1) {
        unsigned char mode[6] = {0, 5, 0, 0, 0, 0};    // 9600 bps 8n1
        Serial_Open(mode);
    }
