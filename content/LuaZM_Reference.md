---
revisions:
- author: Flyingfish
  comment: /\* run() \*/
  timestamp: '2013-05-01T21:57:40Z'
title: LuaZM_Reference
---

LuaZM for the Prizm is under active development, but does not yet have a
released beta. Stay tuned.

## Base Library {#base_library}

The base Lua library has been expanded with a few Prizm-specific
functions to make life easier.

### run()

    run()
    run("file.lua")
    run("compressed.lc")

Run a Lua script. With no arguments, run will launch a file browser.
With arguments, it will run the specified plain-text or compressed Lua
program from Flash memory.

This function can also run files in folders, just use the backslash
(\\). However, you must enclose the string in double braces (\[\[,\]\])
so that it does not expect a C-style escape code.

Will not work:

    > run("folder\file.lua")
    > error: invalid escape sequence

Will work:

`> run([[folder\file.lua]])`

### about()

Displays the about dialog, giving Lua credits.

## ZMG Library {#zmg_library}

All functions in the ZMG ("Pri**ZM** **G**raphics) library are invoked
with zmg.*function*(*arguments,...*). The functions with filled-in
details and arguments have been implemented, but most are untested.
Functions without details are unimplemented. They are as follows:

### zmg.clear()

`zmg.clear()`

Clears the VRAM buffer. No arguments, no return value.

### zmg.copySprite()

`zmg.copysprite(x,y,width,height,data)`

Copies the sprite to the VRAM buffer, overwriting any existing contents.
Clips to the current clipping rectangle; if clipRect has not been
called, the edges of the screen are used for clipping.

*x*: (number) x-coordinate of left edge of sprite\
*y*: (number) y-coordinate of top edge of sprite\
*width*: (number) width of sprite in pixels\
*height*: (number) height of sprite in pixels\
*data*: (string) hex for sprite. 4 hex characters per pixel, 5-6-5 color

### zmg.copySpriteMask()

`zmg.copyspritemask(x,y,width,height,mask_color,data)`

Copies the sprite to the VRAM buffer, overwriting any existing contents.
One special color is set as the mask color; any pixels of the sprite
with that color are left as transparent instead of being drawn. Clips to
the current clipping rectangle; if clipRect has not been called, the
edges of the screen are used for clipping.

*x*: (number) x-coordinate of left edge of sprite\
*y*: (number) y-coordinate of top edge of sprite\
*width*: (number) width of sprite in pixels\
*height*: (number) height of sprite in pixels\
*mask_color*: (number) 16-bit color code marking transparent areas of
sprites. See drawCircle() for details. *data*: (string) hex for sprite.
4 hex characters per pixel, 5-6-5 color

### zmg.drawLine()

`zmg.drawLine(x1, y1, x2, y2, color)`

Draws a line of color color from (x1, y1) to (x2, y2). Clips to the
current clipping rectangle; if clipRect has not been called, the edges
of the screen are used for clipping.

*x1*: (number) x-coordinate of first point\
*y1*: (number) y-coordinate of first point\
*x2*: (number) x-coordinate of second point\
*y2*: (number) y-coordinate of second point\
*color*: (number) 16-bit color code. See drawcircle() for details.

### zmg.drawPoint()

`zmg.drawPoint(x,y,color)`

Draw a pixel of color color at (x,y). Clips to the current clipping
rectangle; if clipRect has not been called, the edges of the screen are
used for clipping. *x*: (number) x-coordinate of pixel\
*y*: (number) y-coordinate of pixel\
*color*: (number) 16-bit color code. See drawcircle() for details.

### zmg.drawRect()

`zmg.drawRectFill(x,y,width,height,color)`

Draw an unfilled rectangle width edges of width width and height height
starting from top-left pixel coordinate (x,y). Clips to the current
clipping rectangle; if clipRect has not been called, the edges of the
screen are used for clipping. Width and height must be positive. *x*:
(number) x-coordinate of top-left corner\
*y*: (number) y-coordinate of top-left corner\
*width*: (number) width of rectangle\
*height*: (number) height of rectangle\
*color*: (number) 16-bit color code. See drawcircle() for details.

### zmg.drawRectFill()

`zmg.drawRectFill(x,y,width,height,color)`

Draw a rectangular fill of width width and height height starting from
top-left pixel coordinate (x,y). Clips to the current clipping
rectangle; if clipRect has not been called, the edges of the screen are
used for clipping. Width and height must be positive. *x*: (number)
x-coordinate of top-left corner\
*y*: (number) y-coordinate of top-left corner\
*width*: (number) width of fill\
*height*: (number) height of fill\
*color*: (number) 16-bit color code. See drawcircle() for details.

### zmg.fastCopy()

`zmg.fastCopy()`

Copy the VRAM to the LCD. Takes no arguments, has no return value.

### zmg.drawCircle()

`zmg.drawCircle(x,y,radius,color)`

Draws an unfilled circle centered at (x,y) with radius radius and line
color color. Clips to the current clipping rectangle; if clipRect has
not been called, the edges of the screen are used for clipping.

*x*: (number) x-coordinate of center of circle. Must be onscreen.\
*y*: (number) y-coordinate of center of circle. Must be onscreen.\
*radius*: (number) radius of circle. May put some portions of circle
offscreen.\
*color*: (number) A color, such as 0xF500. Not a string. Works with
zmg.makeColor() function.

### zmg.drawCircleFilled()

`zmg.drawCircleFilled(x,y,radius,color)`

Draws a filled circle centered at (x,y) with radius radius and line
color color. Clips to the current clipping rectangle; if clipRect has
not been called, the edges of the screen are used for clipping. Does not
draw an outline; if you want a filled outlined circle, call
drawCircleFilled followed up drawCircle.

*x*: (number) x-coordinate of center of circle. Must be onscreen.\
*y*: (number) y-coordinate of center of circle. Must be onscreen.\
*radius*: (number) radius of circle. May put some portions of circle
offscreen.\
*color*: (number) A color, such as 0xF500. Not a string. Works with
zmg.makeColor() function.

### zmg.drawText()

`zmg.drawText(x,y,string,fgcolor,bgcolor)`

Draw a string in the calculator's PrintMini glyph font with top-left
corner (x,y). Off-screen x/y is allowed. The clipping rectangle is *not*
obeyed.

*x*: (number) x-coordinate of top-left of string of text\
*y*: (number) y-coordinate of top-left of string of text\
*string*: (string) the string to display\
*fgcolor*: (number) Foreground text color. Not a string.\
*bgcolor*: (number) Background text color. Not a string.

### zmg.clipRect()

`zmg.clipRect(x0,y0,width,height)`

Set the clipping rectangle for drawing operations, which defaults to
precisely matching the full LCD. Affects drawCircle, drawRectFill,
drawRect, drawPoint, drawLine, copySprite, and copySpriteMask.

*x0*: (number) x-coordinate of top-left of clipping rectangle. Must be
on-screen.\
*y0*: (number) y-coordinate of top-left of clipping rectangle. Must be
on-screen.\
*width*: (number) width of clipping rectangle. Must be positive.\
*height*: (number) height of clipping rectangle. Must be positive.

### zmg.makeColor()

    zmg.makeColor(r,g,b)
    zmg.makeColor("colorname")

Generate a 16-bit color useable for any of the ZMG routines that require
a color. Returns an integer between 0 and 65535 representing a so-called
R5-G6-B5 color.

#### `makeColor(r,g,b)`

When called with three arguments, it turns red, green,
and blue values, each between 0 and 255, into a 5-6-5 color.

r (number)
: Red component of color, 0-255

g (number)
: Green component of color, 0-255

b (number)
: Blue component of color, 0-255

#### `makeColor("colorname")`

When called
with a single argument, which must be a string, it returns the 5-6-5
color corresponding to that name, or -1 for an unknown color.

Valid values are:

<!--
<ul style="border: 1px solid #aaa; padding: 4px; display: flex; flex-wrap: wrap; list-style-position: inside; column-gap: 1em;">
-->
<ul style="display: grid; grid-template-columns: repeat(auto-fit, 8em); list-style-position: inside;">
  <li>aqua</li>
  <li>azure</li>
  <li>beige</li>
  <li>black</li>
  <li>blue</li>
  <li>blueviolet</li>
  <li>brown</li>
  <li>chartreuse</li>
  <li>chocolate</li>
  <li>coral</li>
  <li>cornsilk</li>
  <li>crimson</li>
  <li>cyan</li>
  <li>darkblue</li>
  <li>darkcyan</li>
  <li>darkgray</li>
  <li>darkgreen</li>
  <li>darkkhaki</li>
  <li>darkorange</li>
  <li>darkorchid</li>
  <li>darkred</li>
  <li>darksalmon</li>
  <li>darkviolet</li>
  <li>deeppink</li>
  <li>dimgray</li>
  <li>firebrick</li>
  <li>forestgreen</li>
  <li>fuchsia</li>
  <li>gold</li>
  <li>goldenrod</li>
  <li>gray</li>
  <li>green</li>
  <li>greenyellow</li>
  <li>honeydew</li>
  <li>hotpink</li>
  <li>indianred</li>
  <li>indigo</li>
  <li>ivory</li>
  <li>khaki</li>
  <li>lavender</li>
  <li>lawngreen</li>
  <li>lightblue</li>
  <li>lightcoral</li>
  <li>lightcyan</li>
  <li>lightgray</li>
  <li>lightgreen</li>
  <li>lightpink</li>
  <li>lightyellow</li>
  <li>lime</li>
  <li>limegreen</li>
  <li>linen</li>
  <li>magenta</li>
  <li>maroon</li>
  <li>midnightblue</li>
  <li>mintcream</li>
  <li>mistyrose</li>
  <li>moccasin</li>
  <li>navy</li>
  <li>oldlace</li>
  <li>olive</li>
  <li>olivedrab</li>
  <li>orange</li>
  <li>orangered</li>
  <li>orchid</li>
  <li>pink</li>
  <li>plum</li>
  <li>powderblue</li>
  <li>purple</li>
  <li>red</li>
  <li>royalblue</li>
  <li>salmon</li>
  <li>sandybrown</li>
  <li>seagreen</li>
  <li>seashell</li>
  <li>sienna</li>
  <li>silver</li>
  <li>skyblue</li>
  <li>slateblue</li>
  <li>slategray</li>
  <li>snow</li>
  <li>springgreen</li>
  <li>steelblue</li>
  <li>tan</li>
  <li>tardisblue</li>
  <li>teal</li>
  <li>thistle</li>
  <li>tomato</li>
  <li>turquoise</li>
  <li>violet</li>
  <li>wheat</li>
  <li>white</li>
  <li>yellow</li>
  <li>yellowgreen</li>
</ul>

### zmg.keyMenu()

`zmg.keyMenu()`

Get a keypress, and handle the \[MENU\] key if pressed. This is
**blocking**, meaning that it will not return until a key is pressed,
and will therefore not return zero. Takes no arguments, returns a
number. The \[ON\] key will cause the Lua interpreter to abort,
analogous to an \[ON\]-break or Ctrl-C. Screenshots can be taken using
the Screen Receiver software while this function is running.

### zmg.keyMenuFast()

`zmg.keyMenuFast()`

Get a keypress, and handle the \[MENU\] key if pressed. This is
non-blocking, and returns zero if no key is pressed. Takes no arguments,
returns a number. The \[ON\] key will cause the Lua interpreter to
abort, analogous to an \[ON\]-break or Ctrl-C.

### zmg.keyDirectPoll()

`zmg.keyDirectPoll()`

Move the current key poll buffer into the previous key poll buffer, and
re-poll the keys. Must be called at least once (ideally twice) before
zmg.keyDirect(key) is called.

### zmg.keyDirect()

`zmg.keyDirect(key)`

Checks whether *key* is currently released, pressed, or held. Does not
allow \[ON\]-breaking, \[MENU\], or screenshots. Returns:

-   0 if it was released during the current and previous polling period.
-   1 if it was held during the previous polling period, then released.
-   2 if it is now pressed, but was not pressed during the previous
    polling period.
-   3 if it has been held for two polling periods.

*key*: (number) The key to check for. Uses KEY_PRGM\_\*-style keycodes
(see below for full list)

### zmg.time()

`zmg.time()`

Get the current Unix epoch time (that is, the number of seconds since
00:00:00 1/1/1970). Uses the Prizm's internal clock, and returns an
integer counting the number of seconds since the beginning of the epoch.

### zmg.ticks()

`zmg.ticks()`

Returns the current value of the Prizm's 64Hz clock, which increments
every 1/128th of a second. May recycle unexpectedly; good for making
games run at a consistent speed regardless of user input.

## Keycodes

    [LEFT]  38     [F1]    79     [SHIFT] 78     [ALPHA] 77
    [DOWN]  37     [F2]    69     [OPTN]  68     [x^2]   67
    [UP]    28     [F3]    59     [VARS]  58     [^]     57
    [RIGHT] 27     [F4]    49     [MENU]  48     [EXIT]  47
                   [F5]    39
                   [F6]    29
     
    [X/O/T] 76     [a b/c] 75
    [log]   66     [F<->D] 65 
    [ln]    56     [(]     55 
    [sin]   46     [)]     45
    [cos]   36     [,]     35 
    [tan]   26     [->]    25
     
    [7]     74     [4]     73     [1]     72     [0]     71
    [8]     64     [5]     63     [2]     62     [.]     61
    [9]     54     [6]     53     [3]     52     [EXP]   51
    [DEL]   44     [*]     43     [+]     42     [(-)]   41
    [AC/ON] 10     [/]     33     [-]     32     [EXE]   31

## Version History {#version_history}

**v0.1 Beta:** *(Release 2012-09-08)* First public beta, first version
of the ZMG library, no OS, IO, or Coroutines libraries.
