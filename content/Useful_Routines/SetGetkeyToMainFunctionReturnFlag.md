---
title: SetGetkeyToMainFunctionReturnFlag
aliases:
  - /SetGetkeyToMainFunctionReturnFlag/
---

This is a custom function written by Simon Lothar as there is no syscall
which directly complements
[GetGetkeyToMainFunctionReturnFlag]({{< ref "Syscalls/GetGetkeyToMainFunctionReturnFlag.md" >}}).
However,
[EnableGetkeyToMainFunctionReturn]({{< ref "Syscalls/EnableGetkeyToMainFunctionReturn.md" >}})
and 
[DisableGetkeyToMainFunctionReturn]({{< ref "Syscalls/DisableGetkeyToMainFunctionReturn.md" >}})
can enable and disable this (set to 1 or 0, respectively).

If SetGetkeyToMainFunctionReturnFlag is set to 0, Main Menu access will
be disabled. Set to 1 to re-enable.

    // the function assumes, that the RAM-pointer to GetkeyToMainFunctionReturnFlag is loaded 
    // immediately by a "Move Immediate Data"-instruction
    unsigned int SetGetkeyToMainFunctionReturnFlag( unsigned int enabled ){
    int addr, addr2;
    // get the pointer to the syscall table
            addr = *(unsigned char*)0x80020071;     // get displacement
            addr++;
            addr *= 4;
            addr += 0x80020070;
            addr = *(unsigned int*)addr;
            if ( addr < 0x80020070 ) return 0x101;
            if ( addr >= 0x81000000 ) return 0x102;
    // get the pointer to syscall 1E99
            addr += 0x1E99*4;
            if ( addr < 0x80020070 ) return 0x103;
            if ( addr >= 0x81000000 ) return 0x104;

            addr = *(unsigned int*)addr;
            if ( addr < 0x80020070 ) return 0x105;
            if ( addr >= 0x81000000 ) return 0x106;

            switch ( *(unsigned char*)addr ){
                    case 0xD0 : // MOV.L @( disp, PC), Rn (REJ09B0317-0400 Rev. 4.00 May 15, 2006 page 216)
                    case 0xD1 :
                    case 0xD2 :
                    case 0xD3 :
                    case 0xD4 :
                    case 0xD5 :
                    case 0xD6 :
                    case 0xD7 :
                    case 0xD8 :
                            addr2 = *(unsigned char*)( addr + 1 );  // get displacement
                            addr2++;
                            addr2 *= 4;
                            addr2 += addr;
                            addr2 &= ~3;

                            if ( addr2 < 0x80020070 ) return 0x107;
                            if ( addr2 >= 0x81000000 ) return 0x108;

                            addr = *(unsigned int*)addr2;
                            if ( ( addr & 0xFF000000 ) != 0x88000000 && ( addr & 0xFF000000 ) != 0x8C000000 ) return 0x109; // Changed to work on fx-CG 50 as well as 10/20.

    // finally perform the desired operation and set the flag:
                            if ( enabled ) *(unsigned char*)addr = 0;
                            else *(unsigned char*)addr = 3;

                            break;

                    default : addr = 0x100;
    }
    return addr;
    }
