---
title: CopySprite
---

## Synopsis

Copy a 16-bit sprite/image to the VRAM buffer.

### Definition

    void CopySprite(color_t* sprite, int x, int y, int width, int height) {
       color_t* VRAM = (color_t*)GetVRAMAddress();
       VRAM += LCD_WIDTH_PX*y + x;
       for(int j=y; j<y+height; j++) {
          for(int i=x; i<x+width; i++) {
             *(VRAM++) = *(sprite++);
          }
          VRAM += LCD_WIDTH_PX-width;
       }
    } 

### Inputs

-   *color_t\** **sprite**: The sprite data (from SourceCoder?)
-   *int* **x**: X-coordinate of top-left of sprite
-   *int* **y**: Y-coordinate of top-left of sprite
-   *int* **width**: Width of sprite, in pixels
-   *int* **height**: Height of sprite, in pixels

### Outputs

Sprite drawn to VRAM

## Comments

[SourceCoder](http://sc.cemetech.net) can be used to convert .png, .bmp,
and .gif images to arrays that you can use with this routine.
