---
bookCollapseSection: true
title: Useful Routines
---

The functions in this section implement useful functionality that isn't
provided by the OS or any of the typical libraries.

Some of the routines provided here come from the Cemetech forum's
[Useful Prizm Routines](http://www.cemetech.net/forum/viewtopic.php?t=6114)
thread, and that thread may have other functions that are not (yet?) here
as well.

## Keyboard Routines {#keyboard_routines}

-   [PRGM_GetKey]({{< ref "Useful_Routines/PRGM_GetKey.md" >}})
-   [Debouncing Multiple-Keypress PRGM
    GetKey]({{< ref "Useful_Routines/Debouncing_Multiple-Keypress_PRGM_GetKey.md" >}})
-   [Text_and_Number_Input]({{< ref "Useful_Routines/Text_and_Number_Input.md" >}})

## Hardware Interface Routines {#hardware_interface_routines}

-   [change_freq]({{< ref "Useful_Routines/change_freq.md" >}})

## Graphical Routines {#graphical_routines}

-   [CopySpriteMaskedAlpha]({{< ref "Useful_Routines/CopySpriteMaskedAlpha.md" >}}) -
    Copy a sprite with an alpha mask onto the screen
-   [HeightColor]({{< ref "Useful_Routines/HeightColor.md" >}}) -
    Convert a value between a maximum and a minimum value into a color.
-   [DrawDialog](DrawDialog) - Draws a DCS SmallWindow-style dialog box
-   [MakeGray]({{< ref "Useful_Routines/MakeGray.md" >}}) - Make a 5-6-5
    color gray
-   [DrawLine]({{< ref "Useful_Routines/DrawLine.md" >}}) - Draws a
    line. Uses the Bresenham line algorithm
-   [Plot]({{< ref "Useful_Routines/Plot.md" >}}) - Draw a point of
    color
-   [FillArea]({{< ref "Useful_Routines/FillArea.md" >}}) - Fill a
    rectangular area of VRAM
-   [Non-blocking_DMA]({{< ref "Useful_Routines/Non-blocking_DMA.md" >}}) -
    Do other stuff while drawing to the screen.

### SourceCoder-Compatible Sprites/Images {#sourcecoder_compatible_spritesimages}

-   [CopySprite]({{< ref "Useful_Routines/CopySprite.md" >}}) - Works
    for 16-bit images and sprites

These routines work particularly well with the 8-bit, 2-bit, and 1-bit
indexed data output from SourceCoder.

-   [CopySpriteNBit]({{< ref "Useful_Routines/CopySpriteNBit.md" >}}) -
    Works for 8-bit, 2-bit, and 1-bit indexed data
-   [CopySpriteMasked2Bit](CopySpriteMasked2Bit) - Treats one of the
    four 2-bit colors as a transparent color
-   [CopySpriteNbitMasked]({{< ref "Useful_Routines/CopySpriteNbitMasked.md" >}}) -
    Treats one color as transparent

### Text

-   [PrintMiniFix]({{< ref "Useful_Routines/PrintMiniFix.md" >}}) -
    Print a text on the screen in mini/small (glyph) font

### Menus

-   [Menu]({{< ref "Useful_Routines/Menu.md" >}}) - Displays a menu
    system similar to Geometry plugin's one and returns the selected
    value.

## Math Routines {#math_routines}

-   [floor]({{< ref "Useful_Routines/floor.md" >}}) - Implementation of
    math.floor (PrizmSDK does not have a math.floor)
