---
title: Flash
aliases:
  - /Flash/
---

The Casio Prizm uses the NOR Flash chip for storing boot code and OS
update code, the OS itself, as well as the Storage Memory. The flash
chip is much slower than the RAM, but is 16 times larger.

## Hardware Specs {#hardware_specs}

The Prizm traditionally included a Spansion flash chip, until [hardware
revision 001V04]({{< ref "Hardware_Revisions.md" >}}), when it was
replaced by a Macronix chip. Possible reasons for the replacement
include the fact that the Spansion part was discontinued, a lower price
on the Macronix part, and/or the fact that at higher input voltages, the
Macronix part has slightly lower access times (90 ns vs. 100 ns).

For some reason, calculators with the Macronix chip (or is it because of
HW version 4?) take about a second longer to
[cold-boot]({{< ref "/OS_Information/" >}}) than older calculators.

[OS 02.00]({{< ref "Versions_and_versioning.md" >}}) includes support
for both Spansion and Macronix flash. A [special version of OS
01.04]({{< ref "Versions_and_versioning.md" >}}) shipped with hardware
version 001V04, possibly to add support for Macronix flash (this version
was not available for download). The Spansion and Macronix flash chips
in question are almost 100% compatible, and the differences do not seem
to be relevant to the basic flash operations. It is not known whether OS
versions older than the special 01.04 are able to run on hardware with
the Macronix part.

### Spansion

-   Manufacturer: Spansion
-   Model No.: S29GL256N10TFI02
-   Size: 256 Megabits / 32 MB
-   Speed: 100ns
-   Package: TSO56
-   Lead free
-   Temp. Rating: -40°C to 85°C
-   Power: 2.7V to 3.6V
-   Protection: Protection features available, but not used on the Prizm
    (first sector is writable).

### Macronix

-   Manufacturer: Macronix
-   Model No.: MX29GL256FLT2I
-   Size: 256 Megabits / 32 MB
-   Speed: 100ns (VCC=2.7\~3.6V), 90ns (VCC=3.0\~3.6V)
-   Package: TSO56
-   Halogen free
-   Temp. Rating: -40°C to 85°C
-   Power: 2.7V to 3.6V
-   Protection: Protection features available, but not used on the Prizm
    (first sector is writable).

## Memory Mapping {#memory_mapping}

The Prizm maps the actual flash chip in a few locations. Firstly, there
are two direct access locations available to access the entire flash:

-   0xA0000000 to 0xA1FFFFFF - Cached
-   0x80000000 to 0x81FFFFFF - Same, but non-cached

Addins are executed from the ROM, with the executable code virtually
mapped to 0x00300000 by the MMU.

## Mapping files to memory {#mapping_files_to_memory}

Some work has been done to develop a way to map storage memory files to
addresses in the memory space, similarly to the way add-ins are mapped.
Such development should be considered alpha-stage; it has been paused
due to a (possibly unrelated) hardware failure. More information in
AHelper's GitHub project: [1](https://github.com/AHelper/ld-prizm)

## Reading Files Directly {#reading_files_directly}

See the [ Using the File
System]({{< ref "Tutorials/Using_the_File_System.md" >}}) tutorial and
look at using the
[Bfile_FindFirst]({{< ref "Syscalls/Bfile/Bfile_FindFirst.md" >}})
function to get the direct address for files.

## Formatting and Mounting {#formatting_and_mounting}

Todo.

## OS Updating {#os_updating}

Todo. Uses Protocol 7 to load flashing payload to RAM, then receives OS.

---

Special thanks to Simon Lothar for his documentation and for anyone else
involved.
