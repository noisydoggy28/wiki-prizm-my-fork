---
title: Hardware Revisions
aliases:
  - /Hardware_Revisions/
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

This page contains information about the four known hardware revisions
used on the Prizm. The hardware goes by the internal name of LY755 (the
ClassPad model fx-CP400, which shares the same CPU, goes by the internal
name of LY777).

Hardware revisions are mostly the same between fx-CG 10 and fx-CG 20
models. In all the hardware revisions, the only differences between the
models, and apart from any PCB inscriptions, are:

-   Two bytes in the [bootloader]({{< ref "CASIOABS.md" >}}) area of the
    [Flash]({{< ref "Flash.md" >}}). The [OS]({{< ref "/OS_Information/" >}})
    checks these bytes to modify its behavior, enabling fx-CG 20
    features selectively.
-   The fx-CG 20 with hardware revision 001V03 contains a different LCD
    controller that makes the screen white cooler when compared to a
    fx-CG 10 of the same revision. More information can be found on the
    [relevant section of the Display
    article]({{< ref "Technical_Documentation/Display.md" >}}).

## 001V01

(Did this one really exist, and if yes, did it even make its way to
consumers?)

## 001V02

Introduced in production lines in September 2010. Probably the first
hardware revision available to consumers.

## 001V03

Introduced in production lines in October 2010 or before.

Changes from previous version:

-   Small black component near the keyboard / battery board connector
    that is probably a capacitor.
-   New [LCD]({{< ref "Technical_Documentation/Display.md" >}})
    controller 3A36-2.

## 001V04

Introduced in production lines in March 2013 or before.

Changes from previous version:

-   Use of Macronix [Flash]({{< ref "Flash.md" >}}) chip;
-   Different MPU oscillator;
-   Differences near the USB and 3-pin ports - the grounds are now
    directly connected;
-   New [LCD]({{< ref "Technical_Documentation/Display.md" >}})
    controller 3A36B-1.

Calculators with this hardware revision appear to take approximately one
second longer to boot.
