---
title: Main Page
bookToC: false
---

<span style="font-size: larger; font-weight: bold;">Welcome to WikiPrizm</span>

To date, although many great applications have been made for the Casio
Prizm color-screen graphing calculator, details are far too sparse and
disorganized, spanning forum threads on several websites, scattered
downloadable documentation in limited formats, and a few reference
pages. We hope to collate and organize all this information into a
simple, clear, and useable reference guide to programming the Casio
Prizm and to getting games and programs for your Prizm.

## Getting Started

-   For Users:
    **[Prizm User Portal]({{< ref "Prizm_User_Portal.md" >}})**
    for **program and game downloads** and **HOWTOs**.
-   For Programmers:
    **[Prizm Programming Portal]({{< ref "Prizm_Programming_Portal.md" >}})**
    for **SDKs**, **tutorials**, a **syscall** reference, and useful
    **routines**.
-   For All: **[Casio Prizm
    Discussions](http://www.cemetech.net/forum/viewforum.php?f=68)**
    for programming **help**, "how do I..." help, and more

## Help This Project

We would appreciate any contributions you can make to improve WikiPrizm.
Among the most urgently-needed additions:

-   Our primary drive is enumerating and providing good documentation
    and examples of the **[System Calls]({{< ref "Syscalls/" >}})** the
    Prizm provides. Please add any items that you can!
-   The **[Useful Prizm
    Routines](http://www.cemetech.net/forum/viewtopic.php?t=6114)**
    thread on the Cemetech forum has some great C routines, but they
    need to be properly organized into the
    **[Useful Routines]({{< ref "Useful_Routines.md" >}})** page.

---

*WikiPrizm is a project started under the auspices of
[Cemetech](http://www.cemetech.net), but welcomes contributions from the
entire Casio and TI programming community without bias.*
