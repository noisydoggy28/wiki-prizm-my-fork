---
revisions:
- author: Ahelper
  comment: Organization
  timestamp: '2012-07-30T19:39:15Z'
title: Zeldaking_Tutorial_1
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

In this section of the tutorial, you’ll learn where to find resources
for learning computer C, as well as set up the Prizm SDK which will
start you coding right away.

## Resources

First off you will need to have to know C, or C++. C is recommended
since coding C++ for the Prizm is very splotchy. If you have no clue
what C is we recommend visiting [1](http://www.cprogramming.com/). If
you have any questions concerning code, become a member on
[Cemetech](http://cemete.ch/) and post on the forums.

## Setting up the SDK and other Files {#setting_up_the_sdk_and_other_files}

Now we are sure you have some coding experience you will need a source
code editor, most preferably Notepad++. This allows you to code in the
easiest way possible. Google or Bing search it and download. Now go to
[here]({{< ref "Prizm_Programming_Portal.md" >}}) and click on something
that looks like this: "PrizmSDK: PrizmSDK vX.X" This will bring you to
the Cemetech website, you can download the .tar or the .zip file. I
prefer the .zip because it is easier to extract.

Find the folder where the Prizm SDK was downloaded (possibly in
Downloads). Right click and select extract all, this will leave you with
the empty .zip file, and a normal folder. The folder has all of the
files you will need. Right click again and select cut, navigate to your
computer C:/ file (local disk C:/). Right click and paste. Now you have
the SDK in the correct place.

## Credits

Zeldaking

Ashbad
